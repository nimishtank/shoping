<?php
$config = array(
        'login' => array(
                array(
                        'field' => 'username',
                        'label' => 'Username',
                        'rules' => 'required'
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                         'rules' => 'required|trim'
                )
        ),
        'registerform' => array(
                array(
                        'field' => 'fname',
                        'label' => 'First Name',
                        'rules' => 'required|min_length[3]|regex_match[/^[0-9]{9}[vVxX]$/]|trim'
                ),
                array(
                        'field' => 'lname',
                        'label' => 'Last Name',
                        'rules' => 'required|min_length[3]|regex_match[/^[0-9]{9}[vVxX]$/]|trim'
                ),
                array(
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email|valid_emails|is_unique[users.email]|trim'
                ),
                array(
                        'field' => 'mobileNo',
                        'label' => 'mobile Number',
                        'rules' => 'required|min_length[3]|regex_match[/^[0-9]{9}[vVxX]$/]|trim'
                ),
                array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required|min_length[8]|trim'
                ),
                array(
                        'field' => 'cpassword',
                        'label' => 'Conform Password',
                        'rules' => 'required|min_length[8]|trim|matches[password]'
                ),
        ),
    );
?>
