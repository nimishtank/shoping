<?php
class SubcategoryModel extends CI_Model{

	public function addData($data)	{
		$this->db->set($data);
		$this->db->insert('subcategory');	
	}

	public function getallrecords(){
		$query = $this->db->query("select subcategory .*,category.category_name from subcategory join category where subcategory.category_id = category.id_category ORDER BY id_subcategory ASC");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function clientgetallrecords($id){
		$query = $this->db->query("select * from subcategory where category_id=".$id);
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}

	public function Updatesubcategory($id)
	{
		$query = $this->db->query("select * from subcategory where id_subcategory=".$id);
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updaterecord($id,$data){
		$this->db->where('id_subcategory',$id);
		$this->db->update('subcategory',$data);
	}

	public function deletedata($id){
		$query = $this->db->query("delete from subcategory where id_subcategory=".$id);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
}
?>