<?php
class Shopmodel extends CI_Model{

	public function allcategoryProducts($id)
	{
		$query = $this->db->query("select * from product where category_id=".$id);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function allsubcategroyProducts($id)
	{
		$query = $this->db->query("select * from product where subcategory_id=".$id. " LIMIT 0,4");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function allsubcategroyRecord($id){
		$query = $this->db->query("select * from product where subcategory_id=".$id);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function allselectcategory($id)
	{
		$query = $this->db->query('SELECT subcategory.*, category.category_name , category.category_image FROM subcategory JOIN category WHERE category_id = id_category AND category_id='.$id);
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function ajaxprice($min,$max,$sub_id,$limit,$offset)
	{
		$query = $this->db->query('SELECT * FROM product WHERE iteml_price BETWEEN "'.$min.'" and "'.$max.'" AND subcategory_id='.$sub_id. " LIMIT ". $limit. " OFFSET " .$offset);
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function getTotalData($min,$max,$sub_id){
		$query = $this->db->query('SELECT * FROM product WHERE iteml_price BETWEEN '.$min.' and '.$max.' AND subcategory_id='.$sub_id);
		//echo $this->db->last_query();exit;
		return $query->num_rows();
	}

	public function ajaxGetDataForPage($page, $limit, $sub_id)
	{
		$offset = $page * $limit;
		$query = $this->db->query('SELECT * FROM product WHERE subcategory_id='. $sub_id ." LIMIT ". $limit ." OFFSET ". $offset);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return [];
		}
	}
	
	public function ajaxDataForPage($page, $limit, $sub_id,$min,$max){
		$offset = ($page * $limit)-7;
		$query = $this->db->query('SELECT * FROM product WHERE iteml_price BETWEEN '.$min.' and '.$max.' AND subcategory_id='.$sub_id. " LIMIT ". $limit. " OFFSET " .$offset);
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return [];
		}
	}	
}
?>