<?php
class Settingmodel extends CI_Model
{
	
	public function getallcategory($categoryId){
		$query = $this->db->query("select * from subcategory where category_id=".$categoryId);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function addslider($data){
		$this->db->set($data);
		$this->db->insert("slider");
	}

	public function getallslides(){
		$query = $this->db->query("SELECT slider .*,category.category_name,subcategory.subcategory_name FROM slider JOIN category JOIN subcategory WHERE slider.category_id = category.id_category AND slider.subcategory_id = subcategory.id_subcategory");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function updateshowrecord($id){
		$query = $this->db->query("select * from slider where id_slider=".$id);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updaterecord($id,$data){
		$this->db->where('id_slider',$id);
		$this->db->update('slider',$data);
	}
	public function headerlogoupdate($id){
		$query = $this->db->query("select * from header_logo where id_header=".$id);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updatelogo($id,$data){
		$this->db->where('id_header',$id);
		$this->db->update('header_logo',$data);
	}
}
?>