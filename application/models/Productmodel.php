<?php
Class Productmodel extends CI_Model{
	public $item_name;
	public $item_description;
	public $category_id;
	public $subcategory_id;
	public $item_code;
	public $item_status;
	public $iteml_price;
	public $item_coler;
	public $Item_size;
	public $item_image;

	public function addItem(){
			// $this->db->get($this);
			$this->db->insert('product', $this);
			//echo $this->db->last_query();exit;
	}

	public function get_allProducts(){
		$query = $this->db->query('SELECT product .*, category.category_name, subcategory.subcategory_name FROM product JOIN category JOIN subcategory WHERE product.category_id = category.id_category AND product.subcategory_id = subcategory.id_subcategory  order by id_item asc' );
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0 ){
			return $query->result();
		}else{
			return false;
		}
	}

	public function updateshowrecord($id){
		$query = $this->db->query('select *from product where id_item='.$id);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updateProdust($id,$data){
		$this->db->where('id_item',$id);
		$this->db->update('product',$data);
		//echo $this->db->last_query();exit;
	}

	public function Deleterecord($id){
		$query = $this->db->query("delete from product where id_item=".$id);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
}
?>