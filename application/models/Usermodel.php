<?php
/**
 * 
 */
class Usermodel extends CI_Model
{
	
	public function addUser($data){
		$this->db->set($data);
		$query = $this->db->insert('users');
		$id = $this->db->insert_id();
		$alldata = $this->db->get_where('users', array('id_user' => $id));
		$count = $this->db->affected_rows();
		if($count > 0){
			return $alldata->row();
		}else{
			return false;
		}
	}

	public function register_email_exists($email)
	{
		$query = $this->db->query("select * from users where email=".$email);
		if($query->num_rows() == 0 ){
			return 'true';
		}else{
			return 'false';
		}
	}

	public function logincheck($username,$password)
	{
		$query = $this->db->query("select *from users where email ='".$username."' and password='".md5($password)."' ");	
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function getallState(){
		$query = $this->db->query("select * from states where  country_id=101  ORDER BY sname ASC");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}	

	public function getcity($id){
		$query = $this->db->query("select * from cities where state_id=".$id);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function getallcity(){
		$query = $this->db->query("select * from cities");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function myAccount($user_id){
		$query = $this->db->query('SELECT users .*,states.sname,cities.cname FROM users JOIN states JOIN cities WHERE users.state_id = states.id AND users.city_id = cities.id and id_user='.$user_id);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function editProfile($id){
		$query = $this->db->query("select * from users where id_user=".$id);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updateUser($data){
		$id = $this->session->userdata('id_user');
		$this->db->where('id_user',$id);
		$this->db->update('users',$data);
		//echo $this->db->last_query();exit;
	}

	public function forgetpassword($email){
		$query = $this->db->query("select * from users where email='". $email."'");
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function password_exist($password){
		$pass = md5($password);
		$query = $this->db->query("select * from users where password= '".$pass."' ");
		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			return $query->row()->id_user;
		}else{
			return false;
		}		
	}

	public function addNewpassword($password){
		$id = $this->session->userdata('id_user');
		$pass = md5($password);
		$this->db->where('id_user',$id);
		$this->db->update('users',array('password'=>$pass));	
	}	
}
?>