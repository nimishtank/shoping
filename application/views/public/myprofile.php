<?php include('header.php');?>
<?php //echo "<pre>";print_r($result);?>
<section class="page-title text-center">
	<div class="container relative clearfix">
		<div class="title-holder">
	      <div class="title-text">
	        <h1 class="uppercase">Register Account</h1>
	      </div>
	    </div>
    </div>
</section> <!-- end page title -->	
<?php //echo "<pre>";print_r($myprofile);?>
<section class="section-wrap login-register pt-0 pb-40">
      <div class="container">
      	<div class="row">
      		<div class="col-sm-10 col-sm-offset-1">
      			<?php if($error = $this->session->flashdata('updateUser')):?>
      				<div class="alert alert-success" role="alert"><?php echo $error;?></div>
      			<?php endif;?>	
      		</div>	
      	</div>
      	<div class="row">
        	<form name="registerform" id="registerform" method="post" action="<?php echo  base_url()?>User_controller/addUser">	
        		  <div class="col-sm-5 col-sm-offset-1 mb-40">   
        		  	<div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" name="fname" id="fname" class="form-group" placeholder="" value="<?php echo $result->fname ?>" readonly>
		            </div> 
		             <div class="form-group">
		              	<label for="text">Last Name</label>
		                <input type="text" name="lname" id="lname" class="form-group" placeholder="" value="<?php echo $result->lname ?>" readonly>
		             </div> 	

		              <div class="form-group">
		              	<label for="textareaAddress">Address</label>
		                <textarea name="address" id="address" class="form-group" readonly><?php echo $result->address ?></textarea>
		             </div>
		              
		             <div class="form-group">
		              	<label for="selectState">State</label>
		                <input type="text" name="state" id="state" class="form-group" placeholder="" value="<?php echo $result->sname;?>" readonly>
		             </div>
		          </div>
		          <div class="col-sm-5">
		             <div class="form-group">
		              	<label for="selectCity">City</label>
		                <input type="text" name="city" id="city" class="form-group" placeholder="" value="<?php echo $result->cname;?>" readonly>
		             </div>
		          	 <div class="form-group">
		              	<label for="text">Zipe Code</label>
		                <input type="text" name="zipcode" id="zipcode" class="form-group" placeholder="" value="<?php echo $result->zipcode;?>" readonly>
		             </div>
		             <div class="form-group">
		              	<label for="text">Telephone No</label>
		                <input type="text" name="mobileNo" id="mobileNo" class="form-group" placeholder="" value="<?php echo $result->mobileno;?>" readonly>
		             </div>
		          	<div class="form-group">
		              	<label for="text">Email</label>
		                <input type="text" name="email" id="email" class="form-group" placeholder="" value="<?php echo $result->email;?>" readonly>
		             </div>
		          </div>
		        </div>
    		</form>
      	</div>
    </section> <!-- end login -->
<?php include('footer.php');?>
<script type="text/javascript">
	$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
    	$("#success-alert").slideUp(500);
	});
</script>