<?php include('header.php');?>
<?php echo $totalnoofpages;?>
<div class="container">
      <ol class="breadcrumb">
        <li>
          <a href="index.html">Home</a>
        </li>
        <li>
          <a href="index.html">Shop</a>
        </li>
        <li class="active" id="pageName">
          Catalog Grid
        </li>
      </ol> <!-- end breadcrumbs -->
    </div>


    <!-- Catalogue -->
    <section class="section-wrap pt-70 pb-40 catalogue">
      <div class="container relative">
        <div class="row">

          <div class="col-md-9 catalogue-col right mb-50">

            <!-- Banner -->
            <div class="banner-wrap relative">
              <img src="<?php echo base_url();?>assets/img/banner.jpg" alt="">
              <div class="hero-holder text-center right-align">
                <div class="hero-lines mb-0">
                  <h1 class="hero-heading white">Women Collection</h1>
                  <h4 class="hero-subheading white uppercase">HOT AND FRESH TRENDS OF THIS YEAR</h4>
                </div>
              </div>
            </div>

            <div class="shop-filter">
              <p class="result-count">Showing: 1-12 of 80 results</p>

              <form class="ecommerce-ordering">
                <select>
                  <option value="default-sorting">Default Sorting</option>
                  <option value="price-low-to-high">Price: high to low</option>
                  <option value="price-high-to-low">Price: low to high</option>
                  <option value="by-popularity">By Popularity</option>
                  <option value="date">By Newness</option>
                  <option value="rating">By Rating</option>
                </select>
              </form>
            </div>

            <div class="clearfix"></div>

                  <!-- Pagination -->
            <!-- <div class="pagination-wrap" id="pagination">
              <nav class="pagination right clear">

              <?php
                for ($i=0; $i < $totalnoofpages; $i++) {
              ?>
                <button onclick="getDataForPage(<?php echo $i; ?>, <?php echo $sub_id; ?>);"><?php echo $i+1; ?></button>
              <?php
              }
              ?>
                </nav>
              </div>
            </div> -->

            <div class="shop-catalogue grid-view left" id="subproducts">

              <div class="row row-10 items-grid">
                <?php if($count > 0 ) {
                 $colCount = 0;     
                foreach($allproduct as $data) : 
                  //echo  $colCount;
                    if($colCount === "0"){
                        $col = '12';
                    }
                    elseif($colCount === "1" ){
                        $col = '6';
                    }else{
                        $col = '4';
                    } 
                  ?>
                <div class="col-md-<?php echo $col;?> col-xs-6">
                  <div class="product-item">
                    <div class="product-img">
                      <?php
                        $image = explode(",",$data->item_image);
                      ?>
                      <a href="#">
                        <img src="<?php echo base_url();?>uploads/products/<?php echo $image[0]?>" alt="">
                        <img src="<?php echo base_url();?>uploads/products/<?php echo $image[1]?>" alt="" class="back-img">
                      </a>
                      <div class="product-actions">
                        <a href="#" class="product-add-to-compare" data-toggle="tooltip" data-placement="bottom" title="Add to compare">
                          <i class="fa fa-exchange"></i>
                        </a>
                        <a href="#" class="product-add-to-wishlist" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">
                          <i class="fa fa-heart"></i>
                        </a>
                      </div>
                      <a href="#" class="product-quickview">Quick View</a>
                    </div>
                    <div class="product-details">
                      <h3>
                        <a class="product-title" href="shop-single-product.html"><?php echo $data->item_name;?></a>
                      </h3>
                      <span class="price">
                        <del>
                          <span>$388.00</span>
                        </del>
                        <ins>
                          <span class="ammount"><?php echo $data->iteml_price;?></span>
                        </ins>
                      </span>
                    </div>
                  </div>
                </div>

              <?php
              $colCount++;
             endforeach;
                } else { ?>
                   <div class="col-md-12 col-sm-12 col-xl-12">
                    <div class="alert alert-warning fade in alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                      <strong>Sorry!</strong> No record found.
                    </div>
                  </div>
                <?php } ?>
              </div> <!-- end row -->
            </div> <!-- end grid mode -->

            <div class="clear"></div>

            <!-- Pagination -->
            <div class="pagination-wrap" id="pagination">
              <p class="result-count">Showing: 1-12 of 80 results</p>
              <nav class="pagination right clear">
                <a href="#"><i class="fa fa-angle-left"></i></a>
                <?php for($i=0; $i<$totalnoofpages; $i++) { ?>
                <!-- <span class="page-numbers current"><?php echo $i+1; ?></span> -->
                <span class="page-numbers" onclick="getDataForPage(<?php echo $i; ?>, <?php echo $sub_id; ?>);"><?php echo $i+1; ?></span>
                <?php }?>
                <!-- <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a> -->
                <a href="#"><i class="fa fa-angle-right"></i></a>
              </nav>
            </div>

          </div> <!-- end col -->

          <!-- Sidebar -->
          <aside class="col-md-3 sidebar left-sidebar">

            <!-- Categories -->
            <div class="widget categories">
              <h3 class="widget-title uppercase" id="category_name">Categories</h3>
              <ul class="list-no-dividers" id="ShowCategory">
              </ul>
            </div>

            <!-- Select size -->
            <div class="widget categories">
              <h3 class="widget-title uppercase">Select size</h3>
              <ul class="list-no-dividers">
                <li>
                  <a href="#">Large</a>
                </li>
                <li>
                  <a href="#">Medium</a>
                </li>
                <li>
                  <a href="#">Small</a>
                </li>
                <li>
                  <a href="#">X-Large</a>
                </li>
                <li>
                  <a href="#">X-Small</a>
                </li>
              </ul>
            </div>

            <!-- Select color -->
            <div class="widget categories">
              <h3 class="widget-title uppercase">Select color</h3>
              <ul class="list-no-dividers">
                <li>
                  <a href="#">White</a>
                </li>
                <li>
                  <a href="#">Grey</a>
                </li>
                <li>
                  <a href="#">Black</a>
                </li>
                <li>
                  <a href="#">Pink</a>
                </li>
                <li>
                  <a href="#">Green</a>
                </li>
              </ul>
            </div>

            <!-- Filter by Price -->
            <div class="widget filter-by-price clearfix">
              <h3 class="widget-title uppercase">Filter by Price</h3>

              <div id="slider-range"></div>
              <p>
                <label for="amount">Price:</label>
                <input type="text" id="amount" readonly style="border:0;">
              </p>
            </div>
          </aside> <!-- end sidebar -->

        </div> <!-- end row -->
      </div> <!-- end container -->
    </section> <!-- end catalogue -->


<?php include('footer.php');?>
<script type="text/javascript">
	$(document).ready(function(){
    allProductsCategory();

		function allProductsCategory(){
      var action_name = window.location.search.substring("=");
      var action_id = action_name.split("=");
      var action = action_id[3];
      if(action === 'subcategory'){
        var sub_id = window.location.search.substring(1);
        var s_id = sub_id.split("=");
        var cate_id = s_id[2];
        var cid = cate_id.split("&");
        var datastring = cid[0];
        //alert(datastring);
      }else{
       var cate_id = window.location.search.substring(1);
       var cat_id = cate_id.split("=");
       var datastring = cat_id[1][0];
      }

      $.ajax({
				type:'GET',
				url:'<?php echo base_url();?>Shop_controller/ajaxcategory',
			  data: "cs_id="+datastring,
				dataType:'json',
        success: function(data){
          var html_code = '';
          var i;
          for(i=0; i<data.length; i++){
            //alert(data[i].id_subcategory);
            var html = '<h3 class="widget-title uppercase" id="category_name">'+data[i].category_name+'</h3>';
            html_code  +='<li>'+
                      '<a href="<?php echo base_url();?>Shop_controller/shopcollections?sub_id='+data[i].id_subcategory+'&cate_id='+data[i].category_id+'&action=subcategory" ">'+ data[i].subcategory_name +'</a>'+
                    '</li>';
              }
            $("#ShowCategory").empty();
            $("#ShowCategory").html(html_code);
            $("#category_name").html(html);
        }
      });
		}
	});
</script>
<!-- <script type="text/javascript">
  $("#slider-range").on("click",function(){
    $("#subproducts").hide();
    var action_name = window.location.search.substring(1);
    var action_id = action_name.split("=");
    var action = action_id[1];
    var sid = action.split("&");
    var sub_id =  sid[0];
    var min = $("#slider-range").slider("values")[0];
    var max = $("#slider-range").slider("values")[1];
    $.ajax({
      type:'GET',
      url:'<?php echo base_url();?>Shop_controller/ajaxprice',
      data:'minvalue='+min + '&maxvalue='+max+ '&subId='+sub_id,
      dataType:'json',
      success:function(data){
        var html_code = '';
        var j;
        $("#subproducts").empty();
        html_code +='<div class="row row-10 items-grid">';
        for(j=0; j<data.length; j++){
          var image = data[j].item_image;
          var allimages = image.split(",");
          var coll = '';
          var pagination_code = '';
            if(data.length == 1){
              var coll = 'col-md-12';
            }else if(data.length == 2){
              var coll = 'col-md-6';
            }else{
              var coll = 'col-md-4';
            }
              html_code += 
                                '<div class="'+coll+' col-xs-6" >'+
                                  '<div class="product-item">'+
                                      '<div class="product-img">'+
                                        '<a href="#">'+
                                        '<img src="../uploads/products/'+allimages[0]+'" alt="">'+
                                        '<img src="../uploads/products/'+allimages[1]+'" alt="" class="back-img">'+
                                        '</a>'+
                                        '<div class="product-actions">'+
                                        '<a href="#" class="product-add-to-compare" data-toggle="tooltip" data-placement="bottom" title="Add to compare">'+
                                          '<i class="fa fa-exchange">'+'</i>'+
                                        '</a>'+
                                        '<a href="#" class="product-add-to-wishlist" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">'+
                                          '<i class="fa fa-heart">'+'</i>'+
                                        '</a>'+                    
                                        '</div>'+
                                        '<a href="#" class="product-quickview"> QuickView </a>'+
                                      '</div>'+
                                      '<div class="product-details">'+
                                        '<h3>'+'<a class="product-title" href="shop-single-product.html">'+data[j].item_name+'</a>'+'</h3>'+
                                        '<span class="price">'+
                                        '<del>'+
                                          '<span>$'+data[j].iteml_price+".00"+'</span>'+
                                        '</del>'+
                                        '<ins>'+
                                          '<span class="ammount">$'+data[j].iteml_price+".00"+'</span>'+
                                        '</ins>'+
                                        '</span>'+
                                      '</div>'+  
                                  '</div>'+
                                '</div>';
              
           } 
        html_code+='</div>';
        $("#subproducts").show();   
        $("#subproducts").append(html_code);
        $("#pagination").empty();
        $("#pagination").append(pagination_code);
      }
    });
});
</script> -->
<script type="text/javascript">
function getDataForPage(page, sub_id) {
 // console.log(page);
  $.ajax({
      type:'GET',
      url:'<?php echo base_url();?>Shop_controller/ajaxGetDataForPage',
      data:'page='+ page + '&sub_id='+sub_id,
      dataType:'json',
      success:function(data){
        var i;
        var html_code= '';
        html_code +='<div class="row row-10 items-grid">';
        for(i=0; i<data.length;i++){
          var image = data[i].item_image;
          var allimages = image.split(",");
          var coll = '';
          if(data.length == 1){
              var coll = 'col-md-4';
            }else if(data.length == 2){
              var coll = 'col-md-6';
            }else{
              var coll = 'col-md-4';
            }
          html_code+='<div class="'+coll+' col-xs-6">'+
                            '<div class="product-item">'+
                              '<div class="product-img">'+
                                '<a href="#">'+
                                  '<img src="../uploads/products/'+allimages[0]+'" alt="">'+
                                  '<img src="../uploads/products/'+allimages[1]+'" alt="" class="back-img">'+
                                '</a>'+
                                '<div class="product-actions">'+
                                  '<a href="#" class="product-add-to-compare" data-toggle="tooltip" data-placement="bottom" title="Add to compare">'+
                                    '<i class="fa fa-exchange"></i>'+
                                  '</a>'+
                                  '<a href="#" class="product-add-to-wishlist" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">'+
                                    '<i class="fa fa-heart">'+'</i>'+
                                  '</a>'+                    
                                '</div>'+
                                '<a href="#" class="product-quickview">Quick View</a>'+
                              '</div>'+
                              '<div class="product-details">'+
                                '<h3>'+
                                  '<a class="product-title" href="shop-single-product.html">Night Party Dress</a>'+
                                '</h3>'+
                                '<span class="price">'+
                                  '<del>'+
                                    '<span>$388.00</span>'+
                                  '</del>'+
                                  '<ins>'+
                                    '<span class="ammount">$'+data[i].iteml_price+".00"+'</span>'+
                                  '</ins>'+
                                '</span>'+
                              '</div>'+
                            '</div>'+
                     '</div>';
        }
        html_code+='</div>';
        $("#subproducts").empty(); 
        $("#subproducts").append(html_code); 
      }
  });
}
</script>
/**************Pagination*****************/
<script type="text/javascript">
  $("#slider-range").on("click",function(){
    $("#subproducts").hide();
    var action_name = window.location.search.substring(1);
    var action_id = action_name.split("=");
    var action = action_id[1];
    var sid = action.split("&");
    var sub_id =  sid[0];
    var min = $("#slider-range").slider("values")[0];
    var max = $("#slider-range").slider("values")[1];
    $.ajax({
      type:'GET',
      url:'<?php echo base_url();?>Shop_controller/ajaxALlprice',
      data:'minvalue='+min + '&maxvalue='+max+ '&subId='+sub_id,
      dataType:'json',
      success:function(data){
      var html_code = '';
      var j;
      html_code+='<nav class="pagination right clear">';
      html_code+= '<a href="#"><i class="fa fa-angle-left"></i></a>';
      $("#subproducts").empty();
      for(j=0;j<data;j++){
        var page = j + 1 ;
          if(j==0){
            html_code+='<span class="page-numbers current" onclick="GetpriceRecord('+page+','+sub_id+','+min+','+max+');">'+page+'</span>';
          }else{
              html_code+='<span class="page-numbers" onclick="GetpriceRecord('+page+','+sub_id+','+min+','+max+');">'+page+'</span>';
          }
        }  
        html_code+='<a href="#"><i class="fa fa-angle-right"></i></a>';
        html_code+='</nav>';  
        $("#pagination").empty();
        $("#pagination").html(html_code);
      }
    });
    
});
</script>


<script type="text/javascript">
  $(document).ready(function(){
    // var action_name = window.location.search.substring(1);
    // var action_id = action_name.split("=");
    // var action = action_id[1].split("&");
    // var actionName = action[0];
    // var min = $("#slider-range").slider("values")[0];
    // var max = $("#slider-range").slider("values")[1];
    // GetpriceRecord(1,actionName,min,max);
    //GetpriceRecord();
  });
  
  function GetpriceRecord(page,sub_id,min,max){
    $.ajax({
    type:'GET',
    url:'<?php echo base_url();?>Shop_controller/ajaxDataForPage',
    data:'page='+ page + '&sub_id='+sub_id + '&min=' +min+ '&max='+max,
    dataType:"json",
    success:function(data){
        var i;
        var html_code_record = '';
        html_code_record +='<div class="row row-10 items-grid">';
        for(i=0; i<data.length;i++){
          var image = data[i].item_image;
          var allimages = image.split(",");
          var coll = '';
          if(data.length == 1){
            var coll = 'col-md-12';
          }else if(data.length == 2){
            var coll = 'col-md-6';
          }else{
            var coll = 'col-md-4';
          }
          html_code_record +='<div class="'+coll+' col-xs-6">'+
                            '<div class="product-item">'+
                              '<div class="product-img">'+
                                '<a href="#">'+
                                  '<img src="../uploads/products/'+allimages[0]+'" alt="">'+
                                  '<img src="../uploads/products/'+allimages[1]+'" alt="" class="back-img">'+
                                '</a>'+
                                '<div class="product-actions">'+
                                  '<a href="#" class="product-add-to-compare" data-toggle="tooltip" data-placement="bottom" title="Add to compare">'+
                                    '<i class="fa fa-exchange"></i>'+
                                  '</a>'+
                                  '<a href="#" class="product-add-to-wishlist" data-toggle="tooltip" data-placement="bottom" title="Add to wishlist">'+
                                    '<i class="fa fa-heart">'+'</i>'+
                                  '</a>'+                    
                                '</div>'+
                                '<a href="#" class="product-quickview">Quick View</a>'+
                              '</div>'+
                              '<div class="product-details">'+
                                '<h3>'+
                                  '<a class="product-title" href="shop-single-product.html">Night Party Dress</a>'+
                                '</h3>'+
                                '<span class="price">'+
                                  '<del>'+
                                    '<span>$388.00</span>'+
                                  '</del>'+
                                  '<ins>'+
                                    '<span class="ammount">$'+data[i].iteml_price+".00"+'</span>'+
                                  '</ins>'+
                                '</span>'+
                              '</div>'+
                            '</div>'+
                     '</div>'; 
            }
          html_code_record+='</div>';
          $("#subproducts").empty(); 
          $("#subproducts").append(html_code_record);
      }
      
    });  
  }
</script>