<?php //echo $this->session->userdata('id_user');?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Aha Shop | Home</title>

  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="">
  
  <!-- Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Maven+Pro:400,700%7CRaleway:300,400,700%7CPlayfair+Display:700' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/magnific-popup.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-icons.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sliders.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css" />
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/select2.min.css" /> 
  <!-- Favicons -->
  <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/img/apple-touch-icon.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/img/apple-touch-icon-114x114.png">


</head>

<body class="relative">
  <main class="content-wrapper oh">

    <!-- Navigation -->
    <header class="nav-type-1">

      <div class="top-bar hidden-sm hidden-xs">
        <div class="container">
          <div class="top-bar-line">
            <div class="row">
              <div class="top-bar-links">
                <?php if(!empty($this->session->userdata('id_user'))) { ?>
                <ul class="col-sm-6 top-bar-acc">
                  <li class="dropdown top-bar-link"><i class="fa fa-user" aria-hidden="true""></i><a href="JavaScript:Void(0);">User</a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url()?>User_controller/myAccount">My Account</a></li>
                        <li><a href="<?php echo base_url();?>User_controller/editProfile">Edite Account</a></li>
                        <li><a href="<?php echo base_url();?>Userlogin_controller/changepassword">Change Password</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">My Wishlist</a></li>
                    </ul>
                  </li>
                  <li class="top-bar-link"><a href="<?php echo base_url()?>Userlogin_controller/logout">Logout</a></li>
                  <li class="top-bar-link"><a href="contact.html">Contact</a></li>
                </ul>
              <?php } else { ?>
                <ul class="col-sm-6 top-bar-acc">
                  <li class="top-bar-link"><a href="<?php echo base_url()?>User_controller/index">Register</a></li>
                  <li class="top-bar-link"><a href="<?php echo base_url()?>Userlogin_controller/index"">Login</a></li>
                  <li class="top-bar-link"><a href="contact.html">Contact</a></li>
                </ul>
              <?php } ?>
                <ul class="col-sm-6 text-right top-bar-currency-language">
                  <li>
                    Currency: <a href="#">USD<i class="fa fa-angle-down"></i></a>
                    <div class="currency-dropdown">
                      <ul>
                        <li><a href="#">USD</a></li>
                        <li><a href="#">EUR</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="language">
                    Language: <a href="#">ENG<i class="fa fa-angle-down"></i></a>
                    <div class="language-dropdown">
                      <ul>
                        <li><a href="#">English</a></li>
                        <li><a href="#">Spanish</a></li>
                        <li><a href="#">German</a></li>
                        <li><a href="#">Chinese</a></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="social-icons">
                      <a href="#"><i class="fa fa-twitter"></i></a>
                      <a href="#"><i class="fa fa-facebook"></i></a>
                      <a href="#"><i class="fa fa-google-plus"></i></a>
                      <a href="#"><i class="fa fa-linkedin"></i></a>
                      <a href="#"><i class="fa fa-vimeo"></i></a>
                    </div>
                  </li>
                </ul>              

              </div>
            </div>
          </div>
          
        </div>
      </div> <!-- end top bar -->
    
      <nav class="navbar navbar-static-top">
        <div class="navigation" id="sticky-nav">
          <div class="container relative">

            <div class="row">

              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <!-- Mobile cart -->
                <div class="nav-cart mobile-cart hidden-lg hidden-md">
                  <div class="nav-cart-outer">
                    <div class="nav-cart-inner">
                      <a href="#" class="nav-cart-icon">2</a>
                    </div>
                  </div>
                </div>
              </div> <!-- end navbar-header -->

              <div class="header-wrap">
                <div class="header-wrap-holder">

                  <!-- Search -->
                  <div class="nav-search hidden-sm hidden-xs">
                    <form method="get">
                      <input type="search" class="form-control" placeholder="Search">
                      <button type="submit" class="search-button">
                        <i class="icon icon_search"></i>
                      </button>
                    </form>
                  </div>

                  <!-- Logo -->
                  <div class="logo-container">
                    <div class="logo-wrap text-center">
                      <a href="index.html">
                        <img class="logo" src="<?php echo base_url();?>assets/img/logo_dark.png" alt="logo">
                      </a>
                    </div>
                  </div>

                  <!-- Cart -->
                  <div class="nav-cart-wrap hidden-sm hidden-xs">
                    <div class="nav-cart right">
                      <div class="nav-cart-outer">
                        <div class="nav-cart-inner">
                          <a href="#" class="nav-cart-icon">2</a>
                        </div>
                      </div>
                      <div class="nav-cart-container">
                        <div class="nav-cart-items">

                          <div class="nav-cart-item clearfix">
                            <div class="nav-cart-img">
                              <a href="#">
                                <img src="<?php echo base_url();?>assets/img/shop/cart_small_1.jpg" alt="">
                              </a>
                            </div>
                            <div class="nav-cart-title">
                              <a href="#">
                                Ladies Bag
                              </a>
                              <div class="nav-cart-price">
                                <span>1 x</span>
                                <span>1250.00</span>
                              </div>
                            </div>
                            <div class="nav-cart-remove">
                              <a href="#"><i class="icon icon_close"></i></a>
                            </div>
                          </div>

                          <div class="nav-cart-item clearfix">
                            <div class="nav-cart-img">
                              <a href="#">
                                <img src="<?php echo base_url();?>assets/img/shop/cart_small_2.jpg" alt="">
                              </a>
                            </div>
                            <div class="nav-cart-title">
                              <a href="#">
                                Sequin Suit longer title
                              </a>
                              <div class="nav-cart-price">
                                <span>1 x</span>
                                <span>1250.00</span>
                              </div>
                            </div>
                            <div class="nav-cart-remove">
                              <a href="#"><i class="icon icon_close"></i></a>
                            </div>
                          </div>

                        </div> <!-- end cart items -->

                        <div class="nav-cart-summary">
                          <span>Cart Subtotal</span>
                          <span class="total-price">$1799.00</span>
                        </div>

                        <div class="nav-cart-actions mt-20">
                          <a href="shop-cart.html" class="btn btn-md btn-dark"><span>View Cart</span></a>
                          <a href="shop-checkout.html" class="btn btn-md btn-color mt-10"><span>Proceed to Checkout</span></a>
                        </div>
                      </div>
                    </div>
                    <div class="menu-cart-amount right">
                      <span>
                        Cart /
                        <a href="#"> $1299.50</a>
                      </span>
                    </div>
                  </div> <!-- end cart -->

                </div>
              </div> <!-- end header wrap -->

              <div class="nav-wrap">
                <div class="collapse navbar-collapse" id="navbar-collapse">
                  
                  <ul class="nav navbar-nav">

                    <li id="mobile-search" class="hidden-lg hidden-md">
                      <form method="get" class="mobile-search relative">
                        <input type="search" class="form-control" placeholder="Search...">
                        <button type="submit" class="search-button">
                          <i class="icon icon_search"></i>
                        </button>
                      </form>
                    </li>

                    <li class="dropdown">
                      <a href="<?php echo base_url();?>Shoping_controller/index">Home</a>
                    </li>

                    <li class="dropdown">
                      <a href="#">Pages</a>
                      <i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown"></i>
                      <ul class="dropdown-menu">
                        <li><a href="about-us.html">About Us</a></li>
                        <li><a href="contact.html">Contact</a></li>
                        <li><a href="login.html">Login</a></li>
                        <li><a href="faq.html">F.A.Q</a></li>
                        <li><a href="404.html">404</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="<?php echo base_url();?>Shop_controller/index" class="dropdown-toggle" data-toggle="dropdown">Women</a>
                      <ul class="dropdown-menu megamenu">
                        <?php $row = $this->Categorymodel->allcategory();
                              //echo "<pre>";print_r($subcategoty); 
                        ?>
                        <li>
                          <div class="megamenu-wrap">
                            <div class="row">
                              <?php foreach ($row as $category) : ?>
                              <div class="col-md-3 megamenu-item">
                                <h6><?php echo $category['category_name']?></h6>
                                <ul class="menu-list">
                                  <?php 
                                  $subcategoty = $this->Subcategorymodel->getallrecords();
                                  foreach($subcategoty as $result):
                                      if($result->category_id === $category['id_category']):
                                    ?>
                                      <li><a href="<?php echo base_url();?>Shop_controller/shopcollections?sub_id=<?php echo $result->id_subcategory?>&cate_id=<?php echo $result->category_id?>&action=subcategory"><?php echo $result->subcategory_name;?></a></li>
                                    <?php endif;  ?>
                                  <?php endforeach;?>
                                </ul>
                              </div>
                              <?php endforeach; ?>  
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li> <!-- end categories -->

                    <li class="dropdown">
                      <a href="#">Blog</a>
                      <i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown"></i>
                      <ul class="dropdown-menu">
                        <li><a href="blog-standard.html">Standard</a></li>
                        <li><a href="blog-single.html">Single Post</a></li>
                      </ul>
                    </li>

                    <li class="dropdown"><a href="#">Shop</a><i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown"></i>
                      <ul class="dropdown-menu">
                        <li><a href="shop-catalog.html">Catalog</a></li>
                        <li><a href="shop-collections.html">Collections</a></li>
                        <li><a href="shop-single-product.html">Single Product</a></li>
                        <li><a href="shop-cart.html">Cart</a></li>
                        <li><a href="shop-checkout.html">Checkout</a></li>
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#">Elements</a>
                      <i class="fa fa-angle-down dropdown-toggle" data-toggle="dropdown"></i>
                      <ul class="dropdown-menu">
                        <li><a href="shortcodes.html">Shortcodes</a></li>
                        <li><a href="typography.html">Typography</a></li>
                      </ul>
                    </li>

                    <li class="mobile-links">
                      <ul>
                        <li>
                          <a href="#">Login</a>
                        </li>
                        <li>
                          <a href="#">My Account</a>
                        </li>
                        <li>
                          <a href="#">My Wishlist</a>
                        </li>
                      </ul>
                    </li>
        
                  </ul> <!-- end menu -->
                </div> <!-- end collapse -->
              </div> <!-- end col -->
          
            </div> <!-- end row -->
          </div> <!-- end container -->
        </div> <!-- end navigation -->
      </nav> <!-- end navbar -->
    </header> <!-- end navigation -->