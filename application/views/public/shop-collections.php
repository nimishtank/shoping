<?php include('header.php');?>
<!-- Collections -->
    <section class="section-wrap collections">
      <div class="container">
        <div class="row row-10" id="showCategoty" >
        </div>
      </div>
    </section> <!-- end collections -->
<?php include('footer.php');?>
<script type="text/javascript">
$(document).ready(function() {
	getALlcategory();
	function getALlcategory(){
		event.preventDefault();
		$.ajax({
            url: '<?php echo base_url();?>Shop_controller/categoryajax',
            type: 'POST',
            async: true,
            cache: false,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var html_code = '';
                var i;
                for(i=0; i<data.length; i++)
                {
                	html_code +='<div class="col-sm-3">'+
                	'<a href="<?php echo base_url();?>Shop_controller/shopcollections?category_id='+data[i].id_category+'&action=category" class="collection-item">'+
						              '<img src="../uploads/'+data[i].category_image+'" alt="">'+
						        		'<div class="overlay">'+
						                '<h2 class="uppercase">'+data[i].category_name+'</h2>'+
						              '</div>'+
						            '</a>'+
						          '</div>';
                }
                $('#showCategoty').append(html_code);

            }

        });
	}
})
</script>