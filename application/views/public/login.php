<?php include('header.php');?>
<div class="form-gap"></div>
	<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	            <div class="panel panel-default">
	              	<div class="panel-body">
					  	<h2 class="text-center">L O G I N</h2>
	                  	<p class="text-center">EASILY USING</p>
	                  	<div class="social_media">
		                  <div class="form-group col-sm-6">
			                  <a href="#" class="btn btn-fb"><i class="fa fa-facebook icon"></i>Login With Facebook</a>
			              </div>
			              <div class="form-group col-sm-6">
			                  <a href="#" class="btn btn-google"><i class="fa fa-google icon"></i>Login With Google</a>
			              </div>
		          		</div>
		          		<p class="text-center" style="padding-top: 33px;font-size: 11px;">- OR USING EMAIL -</p>
		          		<?php  if($error = $this->session->flashdata('loginfail')) :?>
				    		<div class="alert alert-danger" role="alert">
				                <?php echo $error;?>
				            </div>
				    	<?php endif;?>
	                  	<div class="panel-body">
	                    	<form name="Loginform" id="Loginform" method="post" autocomplete="off" action="<?php echo  base_url()?>Userlogin_controller/logincheck">	
	                      		<div class="form-group has-feedback">
	                        		<input type="text" name="email" id="email" class="form-group" data-placeholder="Username"  placeholder="Email" >
						  		</div>
								<div class="form-group has-feedback">
	                        		<input type="password" name="password" id="password" class="form-group" placeholder="Password">
	                        		<a href="<?php echo site_url('Userlogin_controller/forgetform');?>" style="float: right;margin-top: -24px;position: relative;margin-bottom:25px;">Forget Password</a>
						  		</div>
	                      		<div class="form-group">
	                      			<input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
	                      		</div>
	                    	</form>	    
	                  	</div>
	              	</div>
	            </div>
	          </div>
		</div>
	</div>
<?php include('footer.php');?>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	$( "#Loginform" ).validate( {
		rules: {
			email: {
				required: true,
				email:true
			},
			password: {
				required: true
			}	
		},
		messages: {
			email: {
				required:"Please enter a valid email address",
				email:"Enter valid E-mial"
			},
			password: "Please provide a password"
			
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );

			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".has-feedback" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".has-feedback" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	} );
	setTimeout(function() {
        $('.alert-danger').fadeOut('fast');
    }, 3500);
});
</script>