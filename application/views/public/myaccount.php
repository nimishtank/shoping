<?php include('header.php');?>
	<div class="container">
	      <ol class="breadcrumb">
	        <li>
	          <a href="<?php echo base_url();?>Shoping_controller/index">Home</a>
	        </li>
	        <li>
	          <a href="<?php echo base_url();?>Shoping_controller/index">Shoping</a>
	        </li>
	        <li class="active">
	          Myaccount
	        </li>
	      </ol> <!-- end breadcrumbs -->
	</div>

	 <section class="section-wrap">
      <div class="container">

        


        <div class="row heading-row mt-50">
          <div class="col-md-12 text-center">
            <h2 class="heading uppercase"><small>Buttons</small></h2>
          </div>
        </div>

        

        <div class="row heading-row mt-50">
          <div class="col-md-12 text-center">
            <h2 class="heading uppercase"><small>My Profile</small></h2>
          </div>
        </div>

        <!-- Forms -->
        <div class="row">
          <div class="col-md-6">

            <input name="text" type="text" placeholder="Input with placeholder">
            <input name="password" id="password" type="password" placeholder="Password input">
            <textarea placeholder="Textarea" rows="3"></textarea>

            <label for="input-label">Input With Label</label>
            <input name="name" id="input-label" type="text">

          </div> <!-- end col -->

          <div class="col-md-6">

            <select>
              <option selected value="default">Select an option</option>
              <option value="green">Green</option>
              <option value="black">Black</option>
              <option value="white">White</option>
            </select>

            <div class="row mt-30">

              <div class="col-md-6 mb-30">
                <h6>Radio Buttons</h6>
                <ul class="radio-buttons">
                  <li>
                    <input type="radio" class="input-radio" name="radio" id="radio1" value="radio1" checked="checked">
                    <label for="radio1">Radio 1</label>
                  </li>
                  <li>
                    <input type="radio" class="input-radio" name="radio" id="radio2" value="radio2">
                    <label for="radio2">Radio 2</label>
                  </li>
                  <li>
                    <input type="radio" class="input-radio" name="radio" id="radio3" value="radio3">
                    <label for="radio3">Radio 2</label>
                  </li>
                </ul>
              </div>

              <div class="col-md-6 mb-30">
                <h6>Checkboxes</h6>
                <ul class="checkboxes">
                  <li>
                    <input type="checkbox" class="input-checkbox" name="checkbox" id="checkbox1" value="1" checked="checked">
                    <label for="checkbox1">Checkbox 1</label>
                  </li>
                  <li>
                    <input type="checkbox" class="input-checkbox" name="checkbox" id="checkbox2" value="2">
                    <label for="checkbox2">Checkbox 2</label>
                  </li>
                  <li>
                    <input type="checkbox" class="input-checkbox" name="checkbox" id="checkbox3" value="3">
                    <label for="checkbox3">Checkbox 3</label>
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </div> <!-- end row -->


        <div class="row heading-row mt-50">
          <div class="col-md-12 text-center">
            <h2 class="heading uppercase"><small>Alert Boxes</small></h2>
          </div>
        </div>

        <!-- Alert Boxed -->
        <div class="row">
          <div class="col-sm-6 mb-50">

            <div class="alert alert-success fade in alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> This is succesful message box.
            </div>

            <div class="alert alert-info fade in alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Information!</strong> This is info message box.
            </div>
           
          </div>

          <div class="col-sm-6">
            <div class="alert alert-warning fade in alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Notice box!</strong> This is notice message box.
            </div>

            <div class="alert alert-danger fade in alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Error!</strong> This is error message box.
            </div>
          </div>     
        </div> <!-- end row -->


        <!-- CTA -->
        <div class="row">
          <div class="col-sm-12">
            <div class="call-to-action">
              <h5 class="uppercase">You want to get this theme now?</h5>
              <a href="#" class="btn btn-lg btn-color"><span>Buy it now</span></a>
            </div>
          </div>
        </div>



      </div>
    </section> <!-- end shortcodes -->
<?php include('footer.php');?>