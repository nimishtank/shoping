<?php include('header.php');?>
	<style type="text/css">
		.form-gap {
		    padding-top: 70px;
		}
		.error {
			float: left !important;
		}
		.form-control-feedback {
		    display: block !important;
		    line-height: 34px !important;
    	}
	</style>
    <!-- login -->
    <div class="form-gap"></div>
	<div class="container">
	    <div class="row">
			<div class="col-md-4 col-md-offset-4">
	            <div class="panel panel-default">
	              <div class="panel-body">
						<div id="msgs"></div>
	                <div class="text-center">
	                  <h3><i class="fa fa-lock fa-4x"></i></h3>
	                  <h2 class="text-center">Forgot Password?</h2>
	                  <p>You can reset your password here.</p>
	                  <div class="panel-body">
	                    <form id="forform" name="forform" role="form" autocomplete="off" class="form" method="post" action="<?php echo  base_url()?>Userlogin_controller/forgetpassword">
	                      <div class="form-group has-feedback">
	                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="">
						    <i class="fa fa-envelope fa-lg form-control-feedback"></i> 
						  </div>
	                      <div class="form-group">
	                      	<input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
	                      </div>
	                    </form>	    
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
		</div>
	</div>

<?php include('footer.php');?>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	$( "#forform" ).validate( {
		rules: {
			email: {
				required: true,
				email:true,
			}	
		},
		messages: {
			email: {
				required:"Please enter a valid email address",
				email:"Enter valid E-mial",
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );

			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});	
});
</script>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	$("#forform").submit(function () {
        //alert("asd");return false;
        event.preventDefault();
        var formData = new FormData($('#forform')[0]);
        $.ajax({
            url: '<?php echo site_url('Userlogin_controller/forgetpassword')?>',
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                console.log(response);
                 if (response === 'success') {
                 	$('#msgs').html('<div class="alert alert-success">Email send success fully </div>').fadeIn().delay(4000).fadeOut('slow');
                } else {
                	$('#msgs').html('<div class="alert alert-danger">Email dose not match </div>').fadeIn().delay(4000).fadeOut('slow');
                }
            }

        });
    });

});
</script>