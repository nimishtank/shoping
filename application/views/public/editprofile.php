<?php include('header.php');?>
<?php //echo "<pre>";print_r($profile);?>
<style type="text/css">
	.error {
		color: red;
	}
</style>
<section class="page-title text-center">
	<div class="container relative clearfix">
		<div class="title-holder">
	      <div class="title-text">
	        <h1 class="uppercase">Update Account</h1>
	      </div>
	    </div>
    </div>
</section> <!-- end page title -->	

<section class="section-wrap login-register pt-0 pb-40">
      <div class="container">
      	<div class="row">
        	<form name="registerform" id="registerform" method="post" action='<?php echo  base_url()?>User_controller/updateUser'>	
        		  <div class="col-sm-5 col-sm-offset-1 mb-40">   
        		  	<div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" name="fname" id="fname" class="form-group" placeholder="" value="<?php echo $profile->fname;?>">
		            </div> 
		             <div class="form-group">
		              	<label for="text">Last Name</label>
		                <input type="text" name="lname" id="lname" class="form-group" placeholder="" value="<?php echo $profile->lname;?>">
		             </div> 	

		              <div class="form-group">
		              	<label for="textareaAddress">Address</label>
		                <textarea name="address" id="address" class="form-group"><?php echo $profile->address;?></textarea>
		             </div>
		              <?php $stateData = $this->Usermodel->getallState();?>
		             <div class="form-group">
		              	<label for="selectState">State</label>
		                 <select name="state" id="state" class="form-control">
		                 	<option value disabled selected>Select State</option>
		                 	<?php foreach($stateData as $row):?>
		                 	<option <?php if($profile->state_id == $row->id){echo "selected";}?> value="<?php echo $row->id;?>"><?php echo $row->sname;?></option>
		                 <?php endforeach;?>
		            	</select>
		             </div>

		             <!-- <div class="form-group">
		              	<label for="selectCity">City</label>
		                <select name="city" id="city" class="form-control">
		                </select>
		             </div>
		     		 
		     		 <div class="form-group">
		              	<label for="text">Zipe Code</label>
		                <input type="text" name="zipcode" id="zipcode" class="form-group" placeholder="" value="">
		             </div> -->

		          </div>
		          <div class="col-sm-5">
		              		<?php //echo"<pre>";print_r($resultcity);?>
		          	 <div class="form-group">
		              	<label for="selectCity">City</label>
		              	<select name="city" id="city" class="form-control">
		              		<option value="">Select city</option>
		              		<?php foreach($resultcity as $datacity):?>
		              			<option <?php if($profile->city_id == $datacity->id){echo "selected";}?> value="<?php echo $datacity->id;?>"><?php echo $datacity->cname;?></option>
		              		<?php endforeach;?>
		                </select>
		             </div><br>
		     		 
		     		 <div class="form-group">
		              	<label for="text">Zipe Code</label>
		                <input type="text" name="zipcode" id="zipcode" class="form-group" placeholder="" value="<?php echo $profile->zipcode; ?>">
		             </div>
		             <div class="form-group">
		              	<label for="text">Telephone No</label>
		                <input type="text" name="mobileNo" id="mobileNo" class="form-group" placeholder="" value="<?php echo $profile->mobileno;?>">
		             </div>
		          	<div class="form-group">
		              	<label for="text">Email</label>
		                <input type="text" name="email" id="email" class="form-group" placeholder="" value="<?php echo $profile->email;?>">
		             </div>
		             <!-- <div class="form-group">
		              	<label for="text">Password</label>
		                <input type="password" name="password" id="password" class="form-group" placeholder="" value="">
		             </div>   
		              <div class="form-group">
		              	<label for="text">Comform password</label>
		                <input type="password" name="cpassword" id="cpassword" class="form-group" placeholder="" value="">
		             </div>  --> 
		              <input type="submit" name="submit" id="submit" value="Update Profiel" class="btn align-middle">
		          </div>
		        </div>
    		</form>
      	</div>
    </section> <!-- end login -->
<?php include('footer.php');?>
<script type="text/javascript">
// $(document).ready(function(){
//     $('#city').select2();
// });	
</script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	$( "#registerform" ).validate( {
		rules: {
			fname: "required",
			lname: "required",
			address: {
				required: true
			},
			state: {
				required: true
			},
			city: {
				required: true
			},
			zipcode:{
				required:true
			},
			email: {
				required: true,
				email: true
			},
			mobileNo: {
				required: true,
				minlength: 10,
				maxlength:10,
 				number: true
			},

			password: {
				required: true,
				minlength: 5
			},
			cpassword: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			}
			
		},
		messages: {
			fname: "Please enter your First Name",
			lname: "Please enter your Last Name",
			address: "Please enter your Address",
			state: "Please select your State",
			city: "Please select your City",
			zipcode: "Please Enter zipcode",
			email: "Please enter a valid email address",
			mobileNo: {
				required: "Please enter a username",
				minlength: "Mobile Number must be  10 Digit",
				minlength: "Mobile Number must be 10 Digit",
				number: "Please Enter valid Mobile Number"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			cpassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );

			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});
});
</script>
<script type="text/javascript">
	$('#state').change(function(){
		var stateid = $('#state').val();
		$.ajax({
			type:'POST',
			data:'stateId='+stateid,
			url:'<?php echo base_url();?>User_controller/getcity',
			async: false,
			dataType: 'json',
			success: function(data){
				var html_code = '';
				var i;
				for(i=0; i<data.length; i++){
					//alert(data[i].id);
					html_code += '<option value="'+data[i].id+'">'+data[i].cname+'</option>';
				}
				//alert(html_code);
				$("#city").html(html_code);	
			}
			
		});
	});
</script>