<?php include('header.php');?>
<style type="text/css">
		.form-gap {
		    padding-top: 70px;
		}
		.error {
			color: red;
		}
		.form-control-feedback {
		    display: block !important;
		    line-height: 34px !important;
    	}
	</style>
    <!-- login -->
    <div class="form-gap"></div>
	<div class="container">
	    <div class="row">
			<div class="col-md-6 col-md-offset-3">
	            <div class="panel panel-default">
	              <div class="panel-body">
						<div id="msgs"></div>
	                <div class="text-center">
	                  <h3><i class="fa fa-pencil-square-o fa-4x"></i></h3>
	                  <h2 class="text-center">Change Password?</h2>
	                  <p>You can reset your password here.</p>
	                  <div class="panel-body">
	                    <form id="changeForm" name="changeForm" role="form" autocomplete="off" class="form" method="post" action="">
	                      <div class="form-group has-feedback">
	                        <input type="password" class="form-control" name="oldPassword" id="oldPassword" placeholder="Old password"><i class="fa fa-lock fa-lg form-control-feedback"></i>
						  </div>
						  <div class="form-group has-feedback">
	                        <input type="password" class="form-control" name="newPassword" id="newPassword" placeholder="New password"><i class="fa fa-lock fa-lg form-control-feedback"></i>
						  </div>
						  <div class="form-group has-feedback">
	                        <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="Confirm password"><i class="fa fa-lock fa-lg form-control-feedback"></i>
						  </div>
	                      <div class="form-group">
	                      	<input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit">
	                      </div>
	                    </form>	    
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
		</div>
	</div>

<?php include('footer.php');?>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$( document ).ready( function () {
	$( "#changeForm" ).validate( {
		rules: {
			oldPassword: {
				required: true,
				remote: {
                    url: "<?php echo base_url()?>Userlogin_controller/password_exists",
                    type: "post"
                }
			},
			newPassword:{
				required: true,
				minlength: 8
			},
			confirmPassword:{
				required: true,
				equalTo: "#newPassword"
			}	
		},
		messages: {
			oldPassword: {
				required: "Please provide a password",
				remote: "Password dose not match"
			},
			newPassword:{
				required: "Please provide a NewPassword",
				minlength: "Your password must be at least 8 characters long"
			},
			confirmPassword:{
				required: "Please enter  ConfirmPassword",
				equalTo: "Password dose not match"
			}
		},
		errorElement: "em",
		errorPlacement: function ( error, element ) {
			// Add the `help-block` class to the error element
			error.addClass( "help-block" );

			if ( element.prop( "type" ) === "checkbox" ) {
				error.insertAfter( element.parent( "label" ) );
			} else {
				error.insertAfter( element );
			}
		},
		highlight: function ( element, errorClass, validClass ) {
			$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
		},
		unhighlight: function (element, errorClass, validClass) {
			$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
		}
	});	
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($){
	$('#changeForm').submit(function(){
		event.preventDefault();
		var formData = new FormData($('#changeForm')[0]);
		//alert(formData);return false;
        $.ajax({
            url: '<?php echo site_url('Userlogin_controller/addnewpassword')?>',
            type: 'POST',
            data: formData,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                //console.log(response);
                 if (response === 'sucess') {
                 	$('#msgs').html('<div class="alert alert-success">Password change success fully. </div>').fadeIn().delay(4000).fadeOut('slow');
                } else {
                	$('#msgs').html('<div class="alert alert-danger">Password  does not change. </div>').fadeIn().delay(4000).fadeOut('slow');
                }
            }

        });
	});
});
</script>