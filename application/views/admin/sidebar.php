<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	
                            <li class="has_sub">
                                <a href="<?php echo base_url()?>Admin_controller"" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> Category </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url();?>Category_controller/index">Add Category</a></li>
                                    <li><a href="<?php echo base_url();?>Category_controller/getallcategory">View Category</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-invert-colors"></i> <span> SubCategory </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url();?>Subcategory_controller/index">Add SubCategory</a></li>
                                    <li><a href="<?php echo base_url();?>Subcategory_controller/getallsubcategory">View subCategory</a></li>
                                </ul>
                            </li>

                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-product-hunt"></i> <span> Products </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url()?>Product_controller">Add Products</a></li>
                                    <li><a href="<?php echo base_url()?>Product_controller/getallProducts">View Products</a></li>
                                </ul>
                            </li>
                      
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="glyphicon glyphicon-shopping-cart"></i><span> Orders </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="admin-sweet-alert.html">All Orders</a></li>
                                    <li><a href="admin-widgets.html">Returns</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="calendar.html" class="waves-effect"><i class="glyphicon glyphicon-user"></i><span> All Users </span></a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cogs"></i><span> Setting </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url();?>Setting_controller/headerlogoupdate?headerId=1">Header Logo Change</a></li>
                                    <li><a href="<?php echo base_url();?>Setting_controller/index">Add Slider</a></li>
                                    <li><a href="<?php echo base_url();?>Setting_controller/allslider">All Slider</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                    <div class="help-box">
                        <h5 class="text-muted m-t-0">For Help ?</h5>
                        <p class=""><span class="text-custom">Email:</span> <br/> support@support.com</p>
                        <p class="m-b-0"><span class="text-custom">Call:</span> <br/> (+123) 123 456 789</p>
                    </div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
