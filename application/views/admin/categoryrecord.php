<?php //echo "<pre>";print_r($getcategory);?>
<?php include('admin_header.php');?>

 			<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">All Category </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">category</a>
                                        </li>
                                        <li class="active">
                                            All category
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

						<div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b><a href="<?php echo base_url()?>Category_controller/index" class="btn btn-primary waves-effect waves-light btn-lg m-b-5">Add New Category</a></b></h4>
                                    
                                    <?php if($error = $this->session->flashdata('updaterecord')) : ?>
                                        <div class="alert alert-success" role="alert">
                                            <?php echo $error;?>
                                        </div>
                                    <?php endif;?>

                                    <?php if($error = $this->session->flashdata('deleterecord')) : ?>
                                        <div class="alert alert-success" role="alert">
                                            <?php echo $error;?>
                                        </div>
                                    <?php endif;?>
                                
                                   <p class="text-muted font-13 m-b-30"></p>
                                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="30%">Category Name</th>
                                            <th width="30%">Image</th>
                                            <th width="15%">Status</th>
                                            <th width="25%">Acation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                            $i = 1;
                                            foreach($getcategory as $row):?>   
                                        <tr>
                                            <td><?php echo $row['category_name']?></td>
                                            <td><img src="../uploads/<?php echo $row['category_image'];?>" class="img-responsive thumb-md" alt="" width="140"></td>
                                            <td><?php echo $row['status']?></td>
                                            <td>
                                                  
                                                <div class="col-md-3">
                                                    <div class="btn-group-vertical m-b-10">
                                                        <button type="button" class="btn btn-lg btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">&nbsp; Acation &nbsp; <span class="caret"></span> </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="<?php echo base_url("Category_controller/edite_category/$row[id_category]")?>">Update Record</a></li>
                                                            <!-- <li><a href="<?php echo base_url("Category_controller/edite_category/id=$row[id_category]")?>">Update Record</a></li> -->
                                                            <li><a href="<?php echo site_url("Category_controller/delete_category/$row[id_category]")?>">Delete Record</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php 
                                            $i++;
                                            endforeach;?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
<?php include('admin_footer.php');?>
<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.keyTable.min.js"></script>

<script>
$(document).ready(function () {
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({keys: true});
    var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
    var table = $('#datatable-fixed-col').DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        }
    });
});
</script>
<script type="text/javascript">
    setTimeout(function() {
        $('.alert-success').fadeOut('fast');
    }, 3500);
</script>