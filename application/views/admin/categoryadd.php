<?php include('admin_header.php')?>

<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Add Category </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Category</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboadrd</a>
                                        </li>
                                        <li class="active">
                                            Add Category
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->   

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-10">
                                            <div class="p-20">
                                                <form role="form" name="categoryForm" id="categoryForm" method="post" enctype="multipart/form-data" action="<?php echo base_url('category_controller/addcategory');?>" data-parsley-validate novalidate>
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Category</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" required parsley-type="categor" name="txtcategory" id="txtcategory" class="form-control"
                                                                   placeholder="Category Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Image</label>
                                                        <div class="col-sm-7">
                                                           <input type="file" name="file" id="file" class="filestyle" data-buttonname="btn-default">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Status</label>
                                                        <div class="col-sm-7">
                                                           <label>
                                                            <input type="radio" name="rdostatus" class="minimal" value = "Enable" checked="checked" /> &nbsp; Enable &nbsp;
                                                            </label>

                                                            <label>
                                                            <input type="radio" name="rdostatus" class="minimal" value = "Disable"/>&nbsp; Disable 
                                                           </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-8 col-sm-offset-4">
                                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">Add
                                                            </button>
                                                            <button type="reset"
                                                                    class="btn btn-default waves-effect m-l-5">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- end row -->

                                </div> <!-- end ard-box -->
                            </div><!-- end col-->

                        </div>   


                    </div> <!-- container -->
                </div> <!-- content -->
<?php include('admin_footer.php');?>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
        $( document ).ready( function () {
            
            $("#categoryForm" ).validate( {
                rules: {
                    txtcategory: {
                        required: true,
                        minlength: 2,
                        remote: {
                            url: "category_controller/register_category_exists",
                            type: "post"    
                        }
                    },
                    file: {
                        required:true,
                        accept:".Jpg,.Png.gif,.jpeg,.png"
                    }
                },
                messages: {
                    txtcategory: {
                        required: "Please enter a Category",
                        minlength: "Your username must consist of at least 2 characters",
                        remote: "Category already in use!"
                    },
                    file: {
                        required:"Please Select  image",
                    accept : "Upload Only .Jpg .Png .gif .jpeg File"
                    }
                },
                errorElement: "label",
                errorPlacement: function ( error, element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".col-sm-7" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".col-sm-7" ).addClass( "has-success" ).removeClass( "has-error" );
                }
            } );

    } );
    </script>