<?php include('admin_header.php')?>
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Add SubCategory </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">SubCategory</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboadrd</a>
                                        </li>
                                        <li class="active">
                                            Add SubCategory
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->   

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">
                                	<?php //echo "<pre>";print_r($row);?>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-10">
                                            <div class="p-20">
                                                <form role="form" name="SubcategoryForm" id="SubcategoryForm" method="post" enctype="multipart/form-data" action="<?php echo base_url('Subcategory_controller/updaterecord?sub_id='.$row->id_subcategory);?>" data-parsley-validate novalidate>
                                                	<div class="form-group row">
                                                        <label for="selectCategory" class="col-sm-4 form-control-label">Category Name</label>
                                                        <div class="col-sm-7">
                                                            <?php $category = $this->Categorymodel->allcategory();
                                                                //echo"<pre>";print_r($category);
                                                            ?>
                                                            <select class="form-control" name="selectCategory" id="selectCategory">
                                                                <option value="">Category Name</option>
                                                                <?php foreach($category as $row_category):?>
                                                                    <option <?php if($row->category_id == $row_category['id_category']){echo "selected";}?> value="<?php echo $row_category['id_category']; ?>"><?php  echo $row_category['category_name'];?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Subcategory Name</label>
                                                        <div class="col-sm-7">
                                                           <input type="text" name="SubCategoryName" id="SubCategoryName" class="form-control" data-buttonname="btn-default" value="<?php echo $row->subcategory_name;?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Image</label>
                                                        <div class="col-sm-7">
                                                           <input type="file" name="file" id="file" class="filestyle" data-buttonname="btn-default">
                                                           <input type="hidden" name="old_file" value="<?php echo $row->image;?>">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Status</label>
                                                        <div class="col-sm-7">
                                                           <input <?php if($row->status == 'Enable') {echo "checked;";}?> type="radio" name="rdostatus" class="minimal" value = "Enable" checked="checked" /> &nbsp; Enable &nbsp;
                                                            </label>
                                                            <label>
                                                            <input <?php if($row->status == 'Disable') {echo "checked";}?> type="radio" name="rdostatus" class="minimal" value = "Disable"/>&nbsp; Disable 
                                                           </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-8 col-sm-offset-4">
                                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">Update
                                                            </button>
                                                            <button type="reset"
                                                                    class="btn btn-default waves-effect m-l-5">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- end row -->

                                </div> <!-- end ard-box -->
                            </div><!-- end col-->

                        </div>   


                    </div> <!-- container -->
                </div> <!-- content -->



<?php include('admin_footer.php')?>