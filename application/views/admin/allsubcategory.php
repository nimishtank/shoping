<?php include('admin_header.php');?>
<div class="content-page" id="shoAllProduct">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">All Subcategory </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">SubCategory</a>
                                        </li>
                                        <li class="active">
                                            All Subcategory
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

						<div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b><a href="<?php echo base_url()?>Subcategory_controller/index" class="btn btn-primary waves-effect waves-light btn-lg m-b-5">Add New SubCategory</a></b></h4>
                                    <?php if($error = $this->session->flashdata('updaterecord')) : ?>
                                        <div class="alert alert-success" role="alert">
                                            <?php echo $error;?>
                                        </div>
                                    <?php endif;?>
                                    <div class="message"></div>
                                   <p class="text-muted font-13 m-b-30"></p>
                                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="20%">Category Name</th>
                                            <th width="20%">SubCategory Name</th>
                                            <th width="25%">SubCategory Image</th>
                                            <th width="15%">Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="showdata">
	                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
<?php include('admin_footer.php');?>
<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.keyTable.min.js"></script>
<script>
function inidatatable(){
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({keys: true});
    var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
    var table = $('#datatable-fixed-col').DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        }
    });
}
</script>
<script type="text/javascript">
    setTimeout(function() {
        $('.alert-success').fadeOut('fast');
    }, 3500);
</script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		showallrecord();
		function showallrecord(){
			$.ajax({
				type: 'POST',
				url:'<?php echo site_url("Subcategory_controller/ajaxallsubcategory")?>',
				async: false,
		  		dataType: 'json',
		  		cache: false,
		  		success:function(data){
		  			console.log(data);
		  			var i;
		  			var html = '';
		  			for(i=0; i<data.length;i++){
		  				//alert(data[i].category_name);
		  				html += '<tr>'+
                                	'<td>'+data[i].category_name+'</td>'+
                                	'<td>'+data[i].subcategory_name+'</td>'+
                                	'<td>'+'<img width="50px" src="../uploads/subcategory/'+data[i].image+'">'+'</td>'+ 
                                	'<td>'+data[i].status+'</td>'+    	
                               		'<td>'+
                                		'<div class="col-md-3">'+
                                            '<div class="btn-group-vertical m-b-10">'+
                                                '<button type="button" class="btn btn-lg btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">&nbsp; Acation &nbsp; '+'<span class="caret">'+'</span>'+'</button>'+
                                                '<ul class="dropdown-menu">'+
                                                    '<li>'+'<a href="<?php echo base_url();?>Subcategory_controller/Updatesubcategory?sub_id='+data[i].id_subcategory+'">Update Record'+'</a>'+'</li>'+
                                                    
                                                    '<li>'+'<a href="javascript:Void(0)" class="subcategory_delete" id="'+data[i].id_subcategory+'">Delete Record'+'</a>'+'</li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                	'</td>'+
                               	'</tr>'; 
		  			}
                    $('#showdata').empty();
		  			$('#showdata').append(html);
                    inidatatable();
		  		}

			});
		}
		$('.subcategory_delete').click(function(){
			var answer = confirm("Do you really want to delete?");
			if(answer){
				var dataId = $(this).attr("id");
				$.ajax({
					type:'GET',
					url:'<?php echo base_url();?>Subcategory_controller/deleterecord',
					data:'sub_id='+dataId,
					async: true,
		            cache: false,
		            contentType: false,
		            processData: false,
					dataType:"json",
					success:function(response){
						if(response.success == true){
							$('.message').html('<div class="alert alert-success">Record delete succes fully. </div>').fadeIn().delay(12000).fadeOut('slow');
							showallrecord();
						}
					}
				});
			}	
		});
	});	
</script>
