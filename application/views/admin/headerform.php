<?php include('admin_header.php')?>
 <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Header Logo </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Header Logo</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboadrd</a>
                                        </li>
                                        <li class="active">
                                            Header Logo
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->   

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-10">
                                        	<div id="message"></div>
                                            <div class="p-20">
                                                <form role="form" name="Headerform" id="Headerform" method="post" enctype="multipart/form-data" action="javascript:void(0);" data-parsley-validate novalidate>
                                                	<input type="hidden" name="header_id" value="<?php echo $row->id_header;?>">
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Name</label>
                                                        <div class="col-sm-7">
                                                        	
                                                            <input type="text" required parsley-type="categor" name="name" id="name" class="form-control"
                                                                   placeholder="Header Name" value="<?php echo $row->name;?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Logo</label>
                                                        <div class="col-sm-7">
                                                           <input type="file" name="file" id="file" class="filestyle" data-buttonname="btn-default">
                                                           <input type="hidden" name="odl_image" value="<?php echo $row->header_logo; ?>">
                                                           <img src="<?php echo base_url()?>uploads/<?php echo $row->header_logo; ?>" style="padding:13px 0px 0px 0px;margin:0px;width:50%;" id="run_Image">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-8 col-sm-offset-4">
                                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">Update
                                                            </button>
                                                            <button type="reset"
                                                                    class="btn btn-default waves-effect m-l-5">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- end row -->

                                </div> <!-- end ard-box -->
                            </div><!-- end col-->

                        </div>   


                    </div> <!-- container -->
                </div> <!-- content -->

<?php include('admin_footer.php');?>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#run_Image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#file").change(function(){
    readURL(this);
});
</script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/additional-methods.min.js"></script>
<script type="text/javascript">
        $( document ).ready( function () {
            var $form = $(this);
            $("#Headerform").validate( {
                rules: {
                    name: {
                        minlength: 2
                    },
                    file: {
                        accept:"png"
                    }
                },
                messages: {
                    name: {
                        required: "Please enter Header Logo Name",
                        minlength: "Your username must consist of at least 2 characters"
                    },
                    file: {
                        required:"Please Select  image",
                        accept : "Upload Only .Png File"
                    }
                },
                errorElement: "label",
                errorPlacement: function ( error, element ) {
                    // Add the `help-block` class to the error element
                    error.addClass( "help-block" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.parent( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).parents( ".col-sm-7" ).addClass( "has-error" ).removeClass( "has-success" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).parents( ".col-sm-7" ).addClass( "has-success" ).removeClass( "has-error" );
                },
                submitHandler: function(form) {
                    var formData = new FormData($('#Headerform')[0]);
                    $.ajax({
                        url: '<?php echo base_url();?>Setting_controller/updateLogo',
                        type: 'POST',
                        data: formData,
                        async: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            //alert (response);return false;
                            if (response === 'sucess') {
                               $('#message').html('<div class="alert alert-success">Header Logo upload  succes fully. </div>').fadeIn().delay(12000).fadeOut('slow');
                                windows.localtion.reload();
                            } else {
                                $('#message').html('<div class="alert alert-danger">Header Logo upload  succes fully. </div>').fadeIn().delay(12000).fadeOut('slow');
                                windows.localtion.reload();
                            }
                        }
                    });
                    return false;
                }
            });

    } );
</script>

