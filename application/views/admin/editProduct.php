<?php include('admin_header.php');?>
<link href="<?php echo base_url();?>assets/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Update Product </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Product </a>
                                        </li>
                                        <li class="active">
                                            Update Product
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->
                         <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">
                                    <?php //echo "<pre>";print_r($row);?>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-10">
                                            <div class="p-20">
                                                <form role="form" name="ProductForm" id="ProductForm" method="post" enctype="multipart/form-data" action="<?php echo site_url('Product_controller/updateProdust?productId='.$row->id_item);?>" data-parsley-validate novalidate>
                                                    <div class="form-group row">
                                                        <label for="inputText" class="col-sm-4 form-control-label">Item Name</label>
                                                        <div class="col-sm-7">
                                                        	<input type="text" required parsley-type="categor" name="txtProductname" id="txtProductname" class="form-control"
                                                                    placeholder="Product Name" value="<?php echo $row->item_name;?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="inputTextarea" class="col-sm-4 form-control-label">Description</label>
                                                        <div class="col-sm-8">
                                                        	<textarea name="txtdescription" id="txtdescription" required parsley-type="categor" class="form-control"
                                                                    placeholder="Descript"><?php echo $row->item_description;?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="selectCategory" class="col-sm-4 form-control-label">Category</label>
                                                        <div class="col-sm-7">
                                                        	<?php $category = $this->Categorymodel->allcategory();
                                                        		//echo"<pre>";print_r($category);
                                                        	?>
	                                                        <select class="form-control" name="selectCategory" id="selectCategory">
	                                                        	<option>Select Category</option>
	                                                        	<?php foreach($category as $row_category):?>
		                                                        	<option <?php if($row->category_id == $row_category['id_category']){echo "selected";}?> value="<?php  echo $row_category['id_category']; ?>"><?php echo $row_category['category_name'];?></option>
		                                                    	<?php endforeach;?>
		                                                    </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="selectCategory" class="col-sm-4 form-control-label">SubCategory</label>
                                                        <div class="col-sm-7">
                                                            <?php $subcategory = $this->SubcategoryModel->getallrecords();
                                                                //echo"<pre>";print_r($subcategory);
                                                            ?>
                                                            <select class="form-control" name="selectSubcategory" id="selectSubcategory">
                                                                <option>Select SubCategory</option>
                                                                <?php foreach($subcategory as $row_subcategory):?>
                                                                    <option <?php if($row->subcategory_id == $row_subcategory->id_subcategory){echo "selected";}?> value="<?php echo $row_subcategory->id_subcategory; ?>"><?php echo $row_subcategory->subcategory_name;?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="addColor" class="col-sm-4 form-control-label">Item Color</label>
                                                        <div class="col-sm-7">
	                                                        <input type="text" name="addColor" id="addColor" value="<?php echo $row->item_coler;?>" data-role="tagsinput" placeholder="add Color"/>
                                                        </div>
                                                    </div>

                                                     <div class="form-group row">
                                                        <label for="addSize" class="col-sm-4 form-control-label">Item Size</label>
                                                        <div class="col-sm-7">
	                                                        <input type="text" name="addSize" id="addSize" value="<?php echo $row->Item_size;?>" data-role="tagsinput" placeholder="add Size"/>
                                                        </div>
                                                    </div>
                                                     <div class="form-group row">
                                                        <label for="inputText" class="col-sm-4 form-control-label">Item Price</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" required parsley-type="categor" id="txtPrice" name="txtPrice" class="form-control"
                                                                    placeholder="Product Price" value="<?php echo $row->iteml_price;?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Image</label>
                                                        <div class="col-sm-7 ">
                                                           <input type="file" name="file[]" id="file" class="filestyle" data-buttonname="btn-default"  multiple="multiple"><br>
                                                           <input type="hidden" name="old_image" value="<?php echo $row->item_image;?>" multiple="multiple">
                                                        </div>
                                                    </div>
                                                    
                                                   <div class="form-group row">
                                                        <label for="inputradio" class="col-sm-4 form-control-label">Status</label>
                                                        <div class="col-sm-7">
                                                           <label>
                                                            <input <?php if($row->item_status == 'Enable') {echo "checked;";}?> type="radio" name="rdostatus" class="minimal" value = "Enable" checked="checked" /> &nbsp; Enable &nbsp;
                                                            </label>

                                                            <label>
                                                            <input <?php if($row->item_status == 'Disable') {echo "checked;";}?> type="radio" name="rdostatus" class="minimal" value = "Disable"/>&nbsp; Disable 
                                                           </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-sm-8 col-sm-offset-4">
                                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">Update
                                                            </button>
                                                            <button type="reset"
                                                                    class="btn btn-default waves-effect m-l-5">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>  
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- end row -->

                                </div> <!-- end ard-box -->
                            </div><!-- end col-->

                        </div>  


                    </div> <!-- container -->

                </div> <!-- content -->
            </div>
<?php include('admin_footer.php');?>
<script src="<?php echo base_url();?>assets/pages/jquery.fileuploads.init.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
<script>
	$(document).ready(function () {
	    if($("#txtdescription").length > 0){
	        tinymce.init({
	            selector: "textarea#txtdescription",
	            theme: "modern",
	            height:300,
	            plugins: [
	                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
	                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	                "save table contextmenu directionality emoticons template paste textcolor"
	            ],
	            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
	            style_formats: [
	                {title: 'Bold text', inline: 'b'},
	                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
	                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
	                {title: 'Example 1', inline: 'span', classes: 'example1'},
	                {title: 'Example 2', inline: 'span', classes: 'example2'},
	                {title: 'Table styles'},
	                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	            ]
	        });
	    }
	});
</script>