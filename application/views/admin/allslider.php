<?php include('admin_header.php');?>
 			<div class="content-page" id="shoAllSlider">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">All Slider </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Slider</a>
                                        </li>
                                        <li class="active">
                                            All slider
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

						<div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <h4 class="m-t-0 header-title"><b><a href="<?php echo base_url()?>Setting_controller/index" class="btn btn-primary waves-effect waves-light btn-lg m-b-5">Add New Slider</a></b></h4>
                                    <?php if($error = $this->session->flashdata('updaterecord')) : ?>
                                        <div class="alert alert-success" role="alert">
                                            <?php echo $error;?>
                                        </div>
                                    <?php endif;?>

                                    <div class="message"></div>
                                
                                   <p class="text-muted font-13 m-b-30"></p>
                                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="15%">Slider Title</th>
                                            <th width="25%">Content</th>
                                            <th width="10%">Image</th>
                                            <th width="15%">Category</th>
                                            <th width="10%">Subcategory</th>
                                            <th width="15%">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody id="showdata">
                                            <!-- <tr>
	                                        	<td>1</td>
	                                        	<td>1</td>
	                                        	<td>1</td>    
	                                        	<td>1</td>
	                                        	<td>1</td>	
	                                       		<td>
	                                        		<div class="col-md-3">
	                                                    <div class="btn-group-vertical m-b-10">
	                                                        <button type="button" class="btn btn-lg btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">&nbsp; Acation &nbsp; <span class="caret"></span> </button>
	                                                        <ul class="dropdown-menu">
	                                                            <li><a href="">Update Slide</a></li>
	                                                            <li><a href="">Delete Slide</a></li>
	                                                        </ul>
	                                                    </div>
	                                                </div>
	                                        	</td>
	                                       	</tr> -->                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                            
                    </div> <!-- container -->
                </div> <!-- content -->
            </div>
<?php include('admin_footer.php');?>

<script src="<?php echo base_url();?>assets/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/datatables/dataTables.keyTable.min.js"></script>
<script>
function inidatatable(){
    $('#datatable').dataTable();
    $('#datatable-keytable').DataTable({keys: true});
    var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
    var table = $('#datatable-fixed-col').DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        }
    });
}
</script>
<script type="text/javascript">
	Allslides();
	function Allslides(){
		$.ajax({
			type:'POST',
			url:'<?php echo  base_url()?>Setting_controller/ajaxallslides',
			async: true,
            cache: false,
            contentType: false,
            processData: false,
            dataType:'json',
			success:function(data){
				console.log(data);
				var html_code = '';
				var i;
				for(i=0; i<data.length; i++){
					html_code +='<tr>'+
								'<td>'+data[i].slider_title +'</td>'+
								'<td>'+data[i].content +'</td>'+
                                '<td>'+'<img width="50px" src="../uploads/slider/'+data[i].slider_image+'">'+'</td>'+
                                '<td>'+data[i].category_name +'</td>'+
                                '<td>'+data[i].subcategory_name +'</td>'+
                                '<td>'+
                                		'<div class="col-md-3">'+
                                            '<div class="btn-group-vertical m-b-10">'+
                                                '<button type="button" class="btn btn-lg btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">&nbsp; Acation &nbsp; '+'<span class="caret"></span>'+ '</button>'+
                                                '<ul class="dropdown-menu">'+
                                                    '<li>'+'<a href="<?php echo base_url();?>Setting_controller/updateshowrecord?slide_id='+data[i].id_slider+'">Update Record</a>'+'</li>'+
                                                    '<li>'+'<a href="JavaScript:Void(0);" class="slide_delete" id="'+data[i].id_slider+'">Delete Record</a>'+'</li>'+
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
	                            '</td>'+
							'</tr>';
				}
				$('#showdata').empty();
                $('#showdata').append(html_code);
                inidatatable(); 
			}
		});
	}
</script>