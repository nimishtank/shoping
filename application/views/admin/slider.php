<?php include('admin_header.php')?>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Add Slider </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Slider</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboadrd</a>
                                        </li>
                                        <li class="active">
                                            Add Slider
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->   

                         <div class="row">
                            <div class="col-xs-12">
                                <div class="card-box">

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12 col-md-10">
                                            <div class="message"></div>
                                            <div class="p-20">
                                                <form role="form" name="SliderForm" id="SliderForm" method="post" enctype="multipart/form-data" action="<?php echo base_url('Setting_controller/addslider');?>" data-parsley-validate novalidate>
                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Slider Title</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" required parsley-type="categor" name="slidertitle" id="slidertitle" class="form-control"
                                                                   placeholder="">
                                                        </div>
                                                    </div>

                                                     <div class="form-group row">
                                                        <label for="inputTextarea" class="col-sm-4 form-control-label">Description</label>
                                                        <div class="col-sm-8">
                                                        	<textarea name="txtdescription" id="txtdescription" required parsley-type="categor" class="form-control"
                                                                    placeholder="Descript"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="hori-pass1" class="col-sm-4 form-control-label">Image</label>
                                                        <div class="col-sm-7">
                                                           <input type="file" name="file" id="file" class="filestyle" data-buttonname="btn-default">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Slider Category</label>
                                                        <div class="col-sm-7">
                                                        	<?php $category = $this->Categorymodel->allcategory();
                                                        		//echo "<pre>";print_r($category);
                                                        	?>
                                                            <select class="form-control" name="selectCategory" id="selectCategory">
	                                                        	<option>Select Category</option>
	                                                        	<?php foreach($category as $row_category):?>
		                                                        	<option value="<?php echo $row_category['id_category']; ?>"><?php echo $row_category['category_name'];?></option>
		                                                    	<?php endforeach;?>
		                                                    </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="inputEmail3" class="col-sm-4 form-control-label">Slider Category</label>
                                                        <div class="col-sm-7">
                                                        	<?php $category = $this->Categorymodel->allcategory();
                                                        		//echo "<pre>";print_r($category);
                                                        	?>
                                                            <select class="form-control" name="selectSubCategory" id="selectSubCategory">
	                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-8 col-sm-offset-4">
                                                            <button type="submit" name="submit" id="submit" class="btn btn-primary waves-effect waves-light">Add
                                                            </button>
                                                            <button type="reset"
                                                                    class="btn btn-default waves-effect m-l-5">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- end row -->

                                </div> <!-- end ard-box -->
                            </div><!-- end col-->

                        </div>   


                    </div> <!-- container -->
                </div> <!-- content -->
<?php include('admin_footer.php');?>
<script>
	$('#selectCategory').change(function(){
        var cat_Id = $('#selectCategory').val();
            $.ajax({
                type:'POST',
                data:'categoryId='+cat_Id,
                url:'<?php echo base_url();?>Setting_controller/getallcategory',
                async: true,
                cache: false,
                processData: false,
                dataType: 'json',
                success: function(data){
                    if(data){    
                        var html_code = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html_code += '<option  value="'+data[i].id_subcategory+'">'+data[i].subcategory_name+'</option>';
                        }
                        $('#selectSubCategory').html(html_code);
                    }
                }
            }); 
    });
</script>

<script>
	$(document).ready(function () {
	    if($("#txtdescription").length > 0){
	        tinymce.init({
	            selector: "textarea#txtdescription",
	            theme: "modern",
	            height:300,
	            plugins: [
	                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
	                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
	                "save table contextmenu directionality emoticons template paste textcolor"
	            ],
	            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
	            style_formats: [
	                {title: 'Bold text', inline: 'b'},
	                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
	                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
	                {title: 'Example 1', inline: 'span', classes: 'example1'},
	                {title: 'Example 2', inline: 'span', classes: 'example2'},
	                {title: 'Table styles'},
	                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
	            ]
	        });
	    }
	});
</script>