<?php
class Shop_controller extends MY_Controller{

	public function index(){
		$this->load->view('public/shop-collections');
	}
	
	public function categoryajax(){
		$this->load->model('categorymodel');
		$data = $this->categorymodel->allcategory();
		echo json_encode($data);
	}
	public function __construct()
	{
		parent::__construct();
		$this->Loadmodel();
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function shopcollections(){
		$action = $this->input->get_post('action');
		if($action === 'category'){
			$id = $this->input->get_post('category_id');
			$this->load->model('Shopmodel');
			$data['allproduct'] = $this->Shopmodel->allcategoryProducts($id);
			$data['count'] = $this->db->affected_rows();
			$this->load->view('public/shop-catalog',$data);
		}else if($action === 'subcategory'){
			$id = $this->input->get_post('sub_id');
			$this->load->model('Shopmodel');
			$data['allproduct'] = $this->Shopmodel->allsubcategroyProducts($id);
			$data['allProductrecord'] = $this->Shopmodel->allsubcategroyRecord($id);
			$data['count'] = $this->db->affected_rows($data['allProductrecord']);
			$data['totalnoofpages'] = ceil($data['count'] / 4);
			$data['sub_id'] = $id;
			$this->load->view('public/shop-catalog',$data);
		}
	}

	public function ajaxcategory()
	{
		$id = $this->input->get('cs_id');
		//echo $id;exit;
		$this->load->model('Shopmodel');
		$catdata = $this->Shopmodel->allselectcategory($id);
		echo json_encode($catdata);
	}

	public function ajaxprice(){
		$this->load->model('Shopmodel');
		$min = $this->input->get('minvalue');
		$max = $this->input->get('maxvalue');
		$sub_id = $this->input->get('subId');
		$Allrecrod = $this->Shopmodel->getTotalData($min,$max,$sub_id);
		$priceData = $this->Shopmodel->ajaxprice($min,$max,$sub_id,0,1);
		echo json_encode($allsubcategroyRecord);
		// echo json_encode($test);
	}

	public function ajaxALlprice(){
		$this->load->model('Shopmodel');
		$min = $this->input->get('minvalue');
		$max = $this->input->get('maxvalue');
		$sub_id = $this->input->get('subId');
		$Allrecrod = $this->Shopmodel->getTotalData($min,$max,$sub_id);
		$Totalpage = ceil($Allrecrod / 7);
		echo json_encode($Totalpage);		
	}

	public function ajaxGetDataForPage(){
		$this->load->model('Shopmodel');
		$page = $this->input->get('page');
		$sub_id = $this->input->get('sub_id');
		$limit = 4;
		$dataForPage = $this->Shopmodel->ajaxGetDataForPage($page, $limit, $sub_id);
		echo json_encode($dataForPage);
	}

	public function ajaxDataForPage(){
		$this->load->model('Shopmodel');
		$page = $this->input->get('page');
		$sub_id = $this->input->get('sub_id');
		$min = $this->input->get('min');
		$max = $this->input->get('max');
		$limit = 7;
		$dataForPage = $this->Shopmodel->ajaxDataForPage($page,$limit,$sub_id,$min,$max);
		echo json_encode($dataForPage);
	}

	public function productcollections(){
		$this->load->view('public/shop-catalog');	
	}

	
}
?>