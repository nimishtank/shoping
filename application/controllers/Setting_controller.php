<?php
class Setting_controller extends MY_Controller{
	
	public function index()
	{
		$this->load->view('admin/slider');
	}

	public function getallcategory()
	{
		$categoryId = $this->input->post('categoryId');
		$this->load->model('Settingmodel');
		$result = $this->Settingmodel->getallcategory($categoryId);
		echo json_encode($result);
	}

	public function addslider(){
		if(isset($_FILES) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
				$config['upload_path']	= './uploads/slider/';
		       	$config['allowed_types']	= 'jpg|jpeg|png|gif';
		       	$config['encrypt_name']	= TRUE;

		       $this->load->library('upload', $config);
		       if($this->upload->do_upload('file')){
		       		$file = $this->upload->data();
		       		//echo "<pre>";print_r($file);
		       		$imageData = $file['raw_name'] . $file['file_ext'];
		       }
		    $data = array(
				'slider_title' =>$this->input->post('slidertitle'),
				'slider_image' =>$imageData,
				'content' =>$this->input->post('txtdescription'),
				'category_id' =>$this->input->post('selectCategory'),
				'subcategory_id' =>$this->input->post('selectSubCategory'),
				'create_at'=> date("Y-m-d H:i:s")
			);
			$this->load->model('Settingmodel');
			$this->Settingmodel->addslider($data);
			return redirect('Setting_controller/allslider');
		}
	}

	public function allslider(){
		$this->load->view('admin/allslider');
	}
	public function ajaxallslides(){
		$this->load->model('Settingmodel');
		$result = $this->Settingmodel->getallslides();
		echo json_encode($result);	
	}

	public function updateshowrecord(){
		$id = $this->input->get('slide_id');
		$this->load->model('Categorymodel');
		$this->load->model('SubcategoryModel');
		$this->load->model('Settingmodel');
		$data['row'] = $this->Settingmodel->updateshowrecord($id); 
		$this->load->view('admin/editeslide',$data);
	}

	public function updaterecord(){
		$id = $this->input->get_post('slide_id');
		if(isset($_FILES) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			$config['upload_path']	= './uploads/slider/';
	       	$config['allowed_types']	= 'jpg|jpeg|png|gif';
	       	$config['encrypt_name']	= TRUE;
		       	$this->load->library('upload', $config);
		       	if($this->upload->do_upload('file')){
		       		$file = $this->upload->data();
		       		//echo "<pre>";print_r($file);
		       		$imageData = $file['raw_name'] . $file['file_ext'];
		       		if(file_exists($imageData)){
		       			unlink($config['upload_path'].$this->input->post('old_file'));
		       		}
		       }
		}else{
			$imageData = $this->input->post('old_file');
		}	
		$data = array(
			'slider_title' =>$this->input->post('slidertitle'),
			'slider_image' =>$imageData,
			'content' =>$this->input->post('txtdescription'),
			'category_id' =>$this->input->post('selectCategory'),
			'subcategory_id' =>$this->input->post('selectSubCategory'),
			'create_at'=> date("Y-m-d H:i:s")
		);
		$this->load->model('Settingmodel');
		$this->Settingmodel->updaterecord($id,$data);
	}

	public function headerlogoupdate(){
		$id = $this->input->get_post('headerId');
		$this->load->model('Settingmodel');
		$data['row'] = $this->Settingmodel->headerlogoupdate($id);
		$this->load->view('admin/headerform',$data);
	}

	public function updateLogo(){
		$id = $this->input->post('header_id');
		if(isset($_FILES) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			$config['upload_path']	= './uploads/';
			$config['allowed_types']	= 'png';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('file')){
	       		$file = $this->upload->data();
	       		$imageData = $file['raw_name'] . $file['file_ext'];
	       		if(file_exists($config['upload_path'].$imageData)){
	       			unlink("uploads\\".$this->input->post('odl_image'));
	       		}
	       }
		}else{
			$imageData = $this->input->post('odl_image');
		}
		$data = array(
			'name' => $this->input->post('name'),
			'header_logo' => $imageData,
			'create_at' => date("Y-m-d H:i:s")); 
		$this->load->model('Settingmodel');
		//echo "<pre>";print_r($data);exit;
		$this->Settingmodel->updatelogo($id,$data);
		echo "sucess";
	}
	public function __construct()
	{
		parent::__construct();
		$this->Loadmodel();
		$this->load->helper('form');
	}
}
?>