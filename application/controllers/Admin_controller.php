<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	public function index(){
		//clear_cache($this);
		$this->load->view('admin/dashboard');
	}

	public function __construct(){
		parent::__construct();
		if( ! $this->session->userdata('user_id')){
			return redirect('Login_controller');
		}

	}
}
?>