<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_controller Extends CI_Controller{

	public function index(){
		$this->load->model('Categorymodel');
		$this->load->model('SubcategoryModel');
		$this->load->view('admin/productadd');
	}

	public function addProdust(){
		if (isset($_FILES) && !empty($_FILES['file']['name']) && isset($_FILES['file']['name']) ) {
		   $imagescount = count($_FILES['file']['name']);
		   $imagess = $_FILES;
		   //echo "<pre>";print_r($imagess);exit;
		   for ($i=0; $i<$imagescount;$i++){
		   
		       $_FILES['file']['name']= $imagess['file']['name'][$i];
		       $_FILES['file']['type']= $imagess['file']['type'][$i];
		       $_FILES['file']['tmp_name']= $imagess['file']['tmp_name'][$i];
		       $_FILES['file']['error']= $imagess['file']['error'][$i];
		       $_FILES['file']['size']= $imagess['file']['size'][$i];

		       $config['upload_path']	= './uploads/products/';
		       $config['allowed_types']	= 'jpg|jpeg|png|gif';
		       $config['encrypt_name']	= TRUE;
		       $this->load->library('upload', $config);
		       $this->upload->initialize($config);

		       if($this->upload->do_upload('file')){
		           // Uploaded images data
		           $imagesData = $this->upload->data();
		           $uploadData[$i] = $imagesData['file_name'];
		       }
		   }
		   $data = $this->input->post();
		   $data['itemImage'] = implode(',',$uploadData);
		   unset($data['submit']);
		   //echo "<pre>";print_r($data);
		   
		   $this->load->model('Productmodel');
		   
		   $this->Productmodel->item_name = $this->input->post('txtProductname');
		   $this->Productmodel->item_description = $this->input->post('txtdescription');
		   $this->Productmodel->category_id = $this->input->post('selectCategory');
		   $this->Productmodel->subcategory_id = $this->input->post('selectSubcategory');
		   $this->Productmodel->item_code =  rand();
		   $this->Productmodel->item_status = 'Enable';
		   $this->Productmodel->iteml_price = $this->input->post('txtPrice');
		   $this->Productmodel->item_coler = $this->input->post('addColor');
		   $this->Productmodel->Item_size = $this->input->post('addSize');
		   $this->Productmodel->item_image = $data['itemImage'];
		   $this->Productmodel->addItem();
		   return redirect('Product_controller/getallProducts');
		}
	}

	public function getallProducts(){
		$this->load->view('admin/allProducts');
	}

	public function AjaxgetallProducts(){
		$this->load->model('Productmodel');
		$result = $this->Productmodel->get_allProducts();
		echo json_encode($result);	
	}

	public function updateshowrecord(){
		$id = $this->input->get('proct_id');
		$this->load->model('Categorymodel');
		$this->load->model('SubcategoryModel');
		$this->load->model('Productmodel');
		$data['row'] = $this->Productmodel->updateshowrecord($id);
		$this->load->view('admin/editProduct',$data);
	}

	public function updateProdust(){
		$id = $this->input->get('productId');
		$uploadData = "";
		//echo "<pre>";print_r($_FILES);
		if(isset($_FILES['file']['error']) && !empty($_FILES['file']['error']) && $_FILES['file']['error'][0]== 0){
			$imagescount = count($_FILES['file']['name']);
			$images = $_FILES;
			for($i=0; $i<$imagescount;$i++){
					//echo $imagescount;
					$_FILES['file']['name'] = $images['file']['name'][$i];
					$_FILES['file']['type'] = $images['file']['type'][$i];
					$_FILES['file']['tmp_name'] = $images['file']['tmp_name'][$i];
					$_FILES['file']['error'] = $images['file']['error'][$i];
					$_FILES['file']['size'] = $images['file']['size'][$i];

			       $config['upload_path']          = './uploads/products/';
			       $config['allowed_types']        = 'jpg|jpeg|png|gif';
			       //echo "<pre>";print_r($config);exit;
			       $this->load->library('upload', $config);
			       $this->upload->initialize($config);

			       if($this->upload->do_upload('file')){
			           $imagesData = $this->upload->data();
			           $uploadData = $uploadData .','. $imagesData['file_name'];
			           $numImage = explode(",",$this->input->post('old_image'));
				       for ($j=0; $j < count($numImage); $j++) { 				       	
					       	if(file_exists("uploads\\products\\".$numImage[$j])){
					       		unlink("uploads\\products\\".$numImage[$j]);
					       	}
				       	}
			        }	    	
			}
		}
		else{
			$uploadData= $this->input->post('old_image');
			//echo"<pew>123";print_r($uploadData);
		}
		$data = array(
			'item_name' =>$this->input->post('txtProductname'),
			'item_description' =>$this->input->post('txtdescription'),
			'category_id' =>$this->input->post('selectCategory'),
			'subcategory_id' =>$this->input->post('selectSubcategory'),
			'item_status' =>$this->input->post('rdostatus'),
			'iteml_price' =>$this->input->post('txtPrice'),
			'item_coler' =>$this->input->post('addColor'),
			'Item_size' =>$this->input->post('addSize'),
			'item_image' =>ltrim($uploadData,","),
			'create_at' =>date("Y-m-d H:i:s")
		);
		//echo "<pre>";print_r($data);exit;
		$this->load->model('Productmodel');
		$this->Productmodel->updateProdust($id,$data);
		return redirect('Product_controller/getallProducts');	
	}
	public function Deleterecord(){
		$id = $this->input->get('itemId');
		$this->load->model('Productmodel');
		$resultdata = $this->Productmodel->Deleterecord($id);
		$mgs['success'] = false;
		if($resultdata){
			$mgs['success'] = true;
		}else{
			$mgs['success'] = false;
		}
		echo json_encode($mgs);
	}
}

?>
