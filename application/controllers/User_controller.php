<?php
/**
 * 
 */
class User_controller extends MY_Controller
{
	public function index(){
		$this->Loadmodel();
		$this->load->model('Usermodel');
		$stateData['state'] = $this->Usermodel->getallState();
		$this->load->view('public/register',$stateData);
	}
	public function addUser()
	{
		$data = array(
			'fname'=>$this->input->post('fname'),
			'lname'=>$this->input->post('lname'),
			'address'=>$this->input->post('address'),
			'state_id'=>$this->input->post('state'),
			'city_id'=>$this->input->post('city'),
			'zipcode'=>$this->input->post('zipcode'),
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password')),
			'mobileno'=>$this->input->post('mobileNo'),
			'create_at'=>date("dd/mm/YY")
		);
		//echo"<pre>";print_r($data);exit;
		$this->load->model('Usermodel');
		$login_id = $this->Usermodel->addUser($data);
		if($login_id){
			$this->session->set_userdata('id_user',$login_id->id_user);
			$this->session->set_userdata('fname',$login_id->fname);
			$this->session->set_userdata('lname',$login_id->lname);
			$this->session->set_userdata('email',$login_id->email);
			$this->session->set_userdata('password',$Login_id->password);
			//echo "<pre>";print_r($this->session);exit;
			return redirect('Shoping_controller/index');
		}
				
	}

	public function getcity(){
		$id = $this->input->post('stateId');
		$this->load->model('Usermodel');
		$cityData= $this->Usermodel->getcity($id);
		echo json_encode($cityData);
	}
	
	public function myAccount(){
		$user_id = $this->session->userdata('id_user');
		$this->load->model('Usermodel');
		$data['result'] = $this->Usermodel->myAccount($user_id);
		//echo"<pre>";print_r($data);exit;
		$this->load->view('public/myprofile',$data);
	}

	public function editProfile(){
		$id = $this->session->userdata('id_user');
		$this->load->model('Usermodel');
		$data['resultcity'] = $this->Usermodel->getallcity();
		$data['profile'] = $this->Usermodel->editProfile($id);
		$this->load->view('public/editProfile',$data);
	}

	public function updateUser(){
		$data = array(
			'fname'=>$this->input->post('fname'),
			'lname'=>$this->input->post('lname'),
			'address'=>$this->input->post('address'),
			'state_id'=>$this->input->post('state'),
			'city_id'=>$this->input->post('city'),
			'zipcode'=>$this->input->post('zipcode'),
			'email'=>$this->input->post('email'),
			'password'=>$this->session->userdata('password'),
			'mobileno'=>$this->input->post('mobileNo'),
			'create_at'=>date("d/m/Y")
		);
		//echo "<pre>";print_r($data);exit;
		$this->load->model('Usermodel');
		$this->Usermodel->updateUser($data);
		return redirect('User_controller/myAccount');
		$this->session->set_flashdata('updateUser',"Profile Update Suceesfully");
	}
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	}
}
?>