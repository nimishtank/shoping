<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_controller extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/categoryadd');
	}
	
	public function addcategory(){
		
		if(isset($_FILES) && isset($_FILES['file']['name']) && empty($_FILES['file']['name'])  == false){
			$config['upload_path']          = './uploads/';
	        $config['allowed_types']        = 'gif|jpg|png';

	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('file'))
                {
                    $error = array('error' => $this->upload->display_errors());

                    $this->load->view('upload_form', $error);
                }
                else
                {
                    $imagedata = $this->upload->data();
                    $category_image = $imagedata['raw_name'] . $imagedata['file_ext'];
                    $data = array(
						'category_name' => $this->input->post('txtcategory'),
						'category_image' => $category_image,     
						'status' => $this->input->post('rdostatus'),
						'created_at' => date("Y-m-d H:i:s")                   		
                    );
                    //echo "<pre>";print_r($data);exit;
                    $this->load->model('categorymodel');
                    $this->categorymodel->insertrecord($data);
                    return redirect('category_controller/getallcategory');
                }
		}
	}

	public function getallcategory(){
		$this->load->model('categorymodel');
		$data['getcategory'] = $this->categorymodel->allcategory();
		$this->load->view('admin/categoryrecord',$data);
	}

	public function register_category_exists(){
		$categoryName = $this->input->post('txtcategory');
		$this->load->model('categorymodel');
		$this->categorymodel->checkcategory($categoryName);
	}

	public function edite_category(){
		$id = $this->uri->segment(3);
		$this->load->model('categorymodel');
		$data['categoryrow'] = $this->categorymodel->editrecord($id);
		$this->load->view('admin/updatecategory',$data);
	}

	public function update_category($id_category){
		if(isset($_FILES) && isset($_FILES['file']['name']) && empty($_FILES['file']['name'])  == false){
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';

			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file')){
				 $error = array('error' => $this->upload->display_errors());
			}else{
				$dataimage = $this->upload->data();
				$category_image = $dataimage['raw_name'].$dataimage['file_ext'];
				unlink($config['upload_path']. $this->input->post('old_file'));
			}
		}else{
			$category_image = $this->input->post('old_file');
		}
			$data= $this->input->post();
			unset($data['submit']);
			//echo "<pre>";print_r($data);exit;
			$data = array(
				'category_name' =>$this->input->post('txtcategory'),
				'category_image' => $category_image,
				'status' =>$this->input->post('rdostatus'),
				'created_at' => date("Y-m-d H:i:s")
			);
			$this->load->model('categorymodel');
			$this->categorymodel->update_record($id_category,$data);
			$this->session->set_flashdata('updaterecord','Record update Sucessfully.');
			return redirect('category_controller/getallcategory');

	}

	public function delete_category($id_category){
		//echo $id_category;
		$this->load->model('categorymodel');
		$this->categorymodel->recorddelete($id_category);
		$this->session->set_flashdata('deleterecord','Delete Record Sucessfully.');
		return redirect('category_controller/getallcategory');
	}

	public function __construct(){
		parent::__construct();
		if( ! $this->session->userdata('user_id')){
			return redirect('Login_controller');
		}

	}
}
?>