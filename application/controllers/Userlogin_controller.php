<?php
class Userlogin_controller extends MY_Controller{
	
	public function index()
	{
		//$this->Loadmodel();
		$this->load->view('public/login');
		//$this->load->view('public/login_form');
	}

	public function logincheck()
	{
		$username = $this->input->post('email'); 
		$password = $this->input->post('password');

		$this->load->model('Usermodel');
		$Login_id = $this->Usermodel->logincheck($username,$password);
		if($Login_id){
			$this->session->set_userdata('id_user',$Login_id->id_user);
			$this->session->set_userdata('fname',$Login_id->fname);
			$this->session->set_userdata('lname',$Login_id->lname);
			$this->session->set_userdata('email',$Login_id->email);
			$this->session->set_userdata('password',$Login_id->password);
			return redirect('Shoping_controller/index');
		}else{
			$this->session->set_flashdata('loginfail','Invalid Username and Password');
			return redirect('Userlogin_controller/index');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('fname');
		$this->session->unset_userdata('lname');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('password');
		return redirect('Shoping_controller/index');
	}

	public function forgetform(){
		$this->load->view('Public/emailform');
	}

	public function forgetpassword(){
		$email = $this->input->post('email');
		//echo $email;exit;
		$this->load->model('Usermodel');
		$result = $this->Usermodel->forgetpassword($email);
		if($result){
			$cust_settings = $this->config->item('sentemail');
			//echo "<pre>";print_r($cust_settings);die();
			$this->load->library('email' ,$cust_settings);
			$this->email->set_newline("\r\n");

			$htmlContent = '<h1>Sending email via SMTP server</h1>';
			$htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

			$this->email->from('nimishtank9@gmail.com', 'Online Shoping');
			$this->email->to('nimishtank9@gmail.com');
			$this->email->subject('Forget Password');
			$this->email->message($htmlContent);
			$this->email->send();
			echo "success";
		}
	}

	public function changepassword(){
		$this->load->view('public/newpassword');
	}

	public function password_exists(){
		$password = $this->input->post('oldPassword');
		$this->load->model('Usermodel');
		$result = $this->Usermodel->password_exist($password);
		if($result > 0 ){
			echo 'true';
        } else {
            echo 'false';
        }
	}

	public function addnewpassword(){
		$password = $this->input->post('newPassword');
		$this->load->model('Usermodel');
		$this->Usermodel->addNewpassword($password);
		echo "sucess";
		
	}

	public function __construct(){
		parent::__construct();
		$this->Loadmodel();
		$this->load->helper('form');
		$this->config->load('sentemail', TRUE);
		//$this->load->library('email');
	}
}
?>