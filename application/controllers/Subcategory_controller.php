<?php
/**
 * 
 */
class Subcategory_controller extends CI_Controller
{
	
	public function index(){
		$this->load->model('Categorymodel');
		$this->load->view('admin/subcategoryform');
	}

	public function addsubcategory(){
		if(!empty($this->input->post())){
			if(isset($_FILES) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
				$config['upload_path']          = './uploads/subcategory/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $this->load->library('upload',$config);
		        if($this->upload->do_upload('file')){
		        	$image = $this->upload->data();
		        	$imagedata = $image['raw_name'].$image['file_ext'];
		        	$data = array(
		        		'category_id' => $this->input->post('selectCategory'),
		        		'subcategory_name' => $this->input->post('SubCategoryName'),
		        		'image'=>$imagedata,
		        		'status' => $this->input->post('rdostatus'),
		        		'create_at' => date("dd/mm/YY")
		        	);
		        	$this->load->model('SubcategoryModel');
		        	$this->SubcategoryModel->addData($data);
		        	return redirect('Subcategory_controller/getallsubcategory');
				}
	        }
		}
	}

	public function getallsubcategory(){
		$this->load->view('admin/allsubcategory');
	}
	public function ajaxallsubcategory(){
		$this->load->model('SubcategoryModel');
		$resultdata = $this->SubcategoryModel->getallrecords();
		echo json_encode($resultdata);
	}

	public function Updatesubcategory()
	{
		$id = $this->input->get('sub_id');
		$this->load->model('Categorymodel');
		$this->load->model('SubcategoryModel');
		$data['row'] = $this->SubcategoryModel->Updatesubcategory($id);
		$this->load->view('admin/Updatesubcategory',$data);
	}

	public function updaterecord()
	{
		$id = $this->input->get('sub_id');
		if(!empty($this->input->post())){
			if(isset($_FILES) && isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
				$config['upload_path']          = './uploads/subcategory/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		        $this->load->library('upload',$config);
		        if($this->upload->do_upload('file')){
		        	$image = $this->upload->data();
		        	$imagedata = $image['raw_name'].$image['file_ext'];
		        	unlink($config['upload_path']. $this->input->post('old_file'));
		        }
			}else{
				$imagedata = $this->input->post('old_file');
			}
			$data = array(
				'category_id' => $this->input->post('selectCategory'),
        		'subcategory_name' => $this->input->post('SubCategoryName'),
        		'image'=>$imagedata,
        		'status' => $this->input->post('rdostatus'),
        		'create_at' => date("dd/mm/YY")
			);
			$this->load->model('SubcategoryModel');
			$this->SubcategoryModel->updaterecord($id,$data);
			$this->session->set_flashdata('updaterecord','Category update success fully.');
			return redirect('Subcategory_controller/getallsubcategory');
		}
	}

	public function deleterecord(){
		$id = $this->input->get('sub_id');
		$this->load->model('SubcategoryModel');
		$resultdata = $this->SubcategoryModel->deletedata($id);
		$mgs['success'] = false;
		if($resultdata){
			$mgs['success'] = true;
		}else{
			$mgs['success'] = false;
		}
		echo json_encode($mgs);
	}

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	}

}
?>