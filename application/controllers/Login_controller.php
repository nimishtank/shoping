
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller {

	public function index(){
		$this->load->helper('form');
		$user_id = $this->session->userdata('user_id');
		if( !empty($user_id)){
			return  redirect('admin_controller');
		}else{	
			$this->load->view('admin/admin_loginform');
		}
	}

	public function admin_login()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if ($this->form_validation->run('login') ){
				$username = $this->input->post('username'); 
			$password = $this->input->post('password');
			$this->load->model('loginmodel');
			$login_id = $this->loginmodel->logincheck($username,$password);
			//echo"<pre>";print_r($login_id);exit;
			if($login_id){
				//echo $login_id->id_admin;exit;
				//$this->load->library('session');
				$this->session->set_userdata('user_id',$login_id->id_admin);
				return redirect('Admin_controller');
			}else{
				$this->session->set_flashdata('login_faild','Invalid Username and Password');
				return redirect('Login_controller'); 
			}
		}else{
			$this->load->view('admin/admin_loginform');
		}
	}

	public function logout(){
		$this->session->unset_userdata('user_id');
		return redirect('Login_controller');
	}
}